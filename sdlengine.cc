
/*
 * sdlengine.cc
 * Copyright (C) 2020 Massimiliano Maniscalco
 * 
 * This is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "sdlengine.hh"
#include <stdio.h>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <pwd.h>
#include "matrix.hh"

int SDLEngine::music_volume = 64;
char SDLEngine::current_music = 0;
std::map<char,Mix_Music*> SDLEngine::musics = std::map<char,Mix_Music*>();

SDLEngine::~SDLEngine() {
  free_memory();
  free_textures();
  free_musics();
  TTF_CloseFont(story_font);
  TTF_CloseFont(title_font);
  TTF_CloseFont(big_font);
  if (renderer) {
    SDL_DestroyRenderer(renderer);
  }
  if (window) {
    SDL_DestroyWindow(window);
  }
  Mix_CloseAudio();
  Mix_Quit();
  TTF_Quit();
  IMG_Quit();
  SDL_Quit();
}

int SDLEngine::start(void) {
  if(init() == false)
    return -1;
 
  SDL_Event event;
 
  while(gamestatus != GameStatus::quit) {
    while(SDL_PollEvent(&event)) {
      if (event.type == SDL_QUIT) {
	gamestatus = GameStatus::quit;
      }
      switch(gamestatus) {
      case GameStatus::title:
	on_title_event(&event);
	break;
      case GameStatus::loading:
	on_loading_event(&event);
	break;
      case GameStatus::starting:
	on_starting_event(&event);
	break;
      case GameStatus::game:
	on_game_event(&event);
	break;
      case GameStatus::endstory:
      case GameStatus::breakstory:
	on_report_event(&event);
	break;
      case GameStatus::saving:
	on_saving_event(&event);
	break;
      }
    }

    switch(gamestatus) {
    case GameStatus::title:
      update_labels();
      titlescroll.update_autoscroll();
      break;
    case GameStatus::loading:
      update_labels();
      break;
    case GameStatus::starting:
      update_labels();
      update_sketches();
      break;
    case GameStatus::game:
      update_sketches(SketchesSchema::waterfall, true);
      update_sprites();
      storyscroll.update();
      update_labels(true);
      try {
	map_labels.at(LABEL_CURRENT)->move();
      } catch (const std::out_of_range& oor) {
	std::cerr << "SDLEngine::start() out of range error" << std::endl;
      }
      gamedraw->animate_pattern(Drawing::Mode::down);
      break;
    case GameStatus::endstory:
    case GameStatus::breakstory:
      update_labels();
      update_sketches(SketchesSchema::fireworks, true);
      break;
    case GameStatus::saving:
      update_labels();
      break;
    }
		
    switch(gamestatus) {
    case GameStatus::title:
      title_render();
      break;
    case GameStatus::loading:
      loading_render();
      break;
    case GameStatus::starting:
      starting_render();
      break;
    case GameStatus::game:
      game_render();
      break;
    case GameStatus::endstory:
    case GameStatus::breakstory:
      report_render();
      break;
    case GameStatus::saving:
      saving_render();
      break;
    }

    if (!Mix_PlayingMusic()) {
      Mix_RewindMusic();
      
    }
  }
  
  return 0;
}

void SDLEngine::create_dirs(void) {
  struct passwd* pw { getpwuid(getuid()) };
  std::string home_path { std::string(pw->pw_dir) };
  std::string local_path { home_path + std::string("/.local/share/storieincomp")};
  save_path.assign(local_path + std::string("/save_slots/"));
  struct stat buffer;
  int result = stat(local_path.c_str(),&buffer);
  if (result != 0) {
    if(mkdir(local_path.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH) != 0) {
      std::cerr << "Couldn't create directory " << local_path << std::endl;
    }
  }
  result = stat(save_path.c_str(),&buffer);
  if (result != 0) {
    if(mkdir(save_path.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH) != 0) {
      std::cerr << "Couldn't create directory " << save_path << std::endl;
    }
  }
}

std::string SDLEngine::find_font_path(const std::string& name, const FcChar8* type) {
  std::string path;
  FcConfig* config = FcInitLoadConfigAndFonts();

  // configure the search pattern, 
  FcPattern* pat = FcNameParse((const FcChar8*)(name.c_str()));
  FcPatternAddString(pat,FC_STYLE,type); // set font style
  if (debugmode) {
    FcPatternPrint(pat); // print FcPattern for debugging
  }
  FcConfigSubstitute(config, pat, FcMatchPattern);
  FcDefaultSubstitute(pat);

  // find the font
  FcResult result;
  FcPattern* font = FcFontMatch(config, pat, &result);
  if (font) {
      FcChar8* file = nullptr;
      if (FcPatternGetString(font, FC_FILE, 0, &file) == FcResultMatch) {
	path.assign(std::string((char*)file));
      }
      FcPatternDestroy(font);
  }
  FcPatternDestroy(pat);
  if (debugmode) {
    std::cout << path << std::endl;
  }
  return path;
}

/*
 * Initialization
 */
bool SDLEngine::init(void) {
  if (debugmode) {
    std::cout << "game files path: " << main_path << std::endl;
  }
  
  // inizializza SDL2
  if(SDL_Init(SDL_INIT_EVERYTHING) != 0) {
    on_SDL_error(std::cerr, "SDL_Init");
    return false;
  }

  // init SDL_image
  if ((IMG_Init(IMG_INIT_PNG) & IMG_INIT_PNG) != IMG_INIT_PNG) {
    on_SDL_error(std::cerr, "IMG_Init");
    return false;
  }

  // init SDL2_ttf
  if (TTF_Init() != 0) {
    on_SDL_error(std::cerr, "TTF_Init");
    return false;
  }

  // load story_font
  FcChar8 rtype[] = "Regular";
  std::string fpath = find_font_path(std::string("OpenSans"), rtype);
  if ((story_font = TTF_OpenFont(fpath.c_str(), FONT_SIZE_STORY)) == nullptr) {
    on_SDL_error(std::cerr, "TTF_OpenFont");
    return false;
  }

  // load title_font
  FcChar8 btype[] = "Bold";
  fpath = find_font_path(std::string("OpenSans"), btype);
  if ((title_font = TTF_OpenFont(fpath.c_str(), FONT_SIZE_TITLE)) == nullptr) {
    on_SDL_error(std::cerr, "TTF_OpenFont");
    return false;
  }
  
  // load bigfont
  FcChar8 sbtype[] = "Semibold";
  fpath = find_font_path(std::string("OpenSans"), sbtype);
  if ((big_font = TTF_OpenFont(fpath.c_str(), FONT_SIZE_BIG)) == nullptr) {
    on_SDL_error(std::cerr, "TTF_OpenFont");
    return false;
  }
    
  // init SDL2_mixer
  if ((Mix_Init(MIX_INIT_OGG) & MIX_INIT_OGG) != MIX_INIT_OGG) {
    on_SDL_error(std::cerr, "Mix_Init");
    return false;
  }

  // initialize Mixer API
  if (Mix_OpenAudio(MIX_DEFAULT_FREQUENCY, MIX_DEFAULT_FORMAT, 2, 4096) != 0) {
    on_SDL_error(std::cerr, "Mix_OpenAudio");
    return false;
  }
  
  // crea una finestra
  if((window = SDL_CreateWindow("Storie Incomplete", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, win_width, win_height, SDL_WINDOW_OPENGL)) == nullptr) {
    on_SDL_error(std::cerr, "SDL_CreateWindow");
    return false;
  }

  // crea un'istanza di SDL_Renderer
  if((renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC | SDL_RENDERER_TARGETTEXTURE)) == nullptr) {
    on_SDL_error(std::cerr, "SDL_CreateRenderer");
    return false;
  }

  // set frame rate
  Timing::set_frames_per_second(60);

  // set up save directory
  create_dirs();
  
  // load game data
  gdata = GameData::load_stories(main_path);

  // unlock first story
  player.unlock_story();

  // set up mixer call back functions
  Mix_HookMusicFinished(music_done);
  Mix_ChannelFinished(channel_spell_done);

  // load musics
  std::string path { main_path + SOUNDS_PATH + std::string("royal-coupling.mp3") };
  Mix_Music* music = Mix_LoadMUS(path.c_str());
  if (music == nullptr) {
    on_SDL_error(std::cerr, "Load music");
    return false;
  } else {
    musics.insert(std::pair<char,Mix_Music*>{ TITLE_MUSIC, music });
  }
  path.assign(main_path + SOUNDS_PATH + std::string("holiday-weasel.mp3"));
  music = Mix_LoadMUS(path.c_str());
  if (music == nullptr) {
    on_SDL_error(std::cerr, "Load music");
    return false;
  } else {
    musics.insert(std::pair<char,Mix_Music*>{ GAME_MUSIC, music });
  }

  // start music
  current_music = TITLE_MUSIC;
  music_volume = TITLE_MUSIC_VOLUME;
  Mix_VolumeMusic(music_volume);
  if (Mix_FadeInMusic(musics.at(current_music), -1, 750) != 0) {
    on_SDL_error(std::cerr, "Play music");
  }
  
  SDL_StopTextInput();
  init_title();

  return true;
}

void SDLEngine::init_title(void) {
  label_index = 0;
  std::vector<std::pair<std::string,double>> vec { { std::string("inizia una nuova avventura"), 0.85 },
						   { std::string("continua un'avventura salvata"), 0.90 } };
  std::vector<std::pair<std::string,double>>::iterator iter = vec.begin();
  while (iter != vec.end()) {
    try {
      Text* lbl = new Text(renderer, iter->first, title_font, COL_TXT_GAME);
      SDL_Rect dest = lbl->get_position();
      dest.x = (win_width - dest.w) / 2;
      dest.y = win_height * iter->second;
      lbl->set_position(&dest);
      labels.push_back(lbl);
    } catch (const Text::TextError& te) {
      std::cerr << "SDLEngine::init_title " << te.get_message() << std::endl;
    }
    iter++;
  }

  try {
    labels.at(label_index)->set_fading_mode(Fading::Mode::in_out_loop);
  } catch (const std::out_of_range& oor) {
    std::cerr << "SDLEngine::init_title() out of range error" << std::endl;
  }

  try {
    Text* lbl = new Text(renderer, std::string("Storie incomplete"), big_font, COL_TXT_GAME);
    SDL_Rect dest = lbl->get_position();
    dest.x = (win_width - dest.w) / 2;
    dest.y = dest.h / 5.0;
    lbl->set_position(&dest);
    lbl->set_fading_mode(Fading::Mode::in);
    lbl->set_alpha(0);
    lbl->set_fading_pace(0.5);
    map_labels.insert(std::pair<char,Text*>{ LABEL_TITLE, lbl });
  } catch (const Text::TextError& te) {
    std::cerr << "SDLEngine::init_title " << te.get_message() << std::endl;
  }

  titlescroll.set_rect({ win_width /20, win_height / 8, win_width - (win_width / 10), win_height / 7 });
  titlescroll.set_max_rows(10);
  
  std::vector<std::string> vec2 { std::string("Game Design"),
				  std::string("Massimiliano Maniscalco"),
				  std::string("Silvio Maniscalco"),
				  std::string(" "),
				  std::string(" "),
				  std::string("Programmato da"),
				  std::string("Massimiliano Maniscalco"),
				  std::string(" "),
				  std::string(" "),
				  std::string("Voce"),
				  std::string("Silvio Maniscalco"),
				  std::string(" "),
				  std::string(" "),
				  std::string("Musiche"),
				  std::string("\"Royal Coupling\" by Kevin MacLeod"),
				  std::string("https://incompetech.filmmusic.io/song/5743-royal-coupling"),
				  std::string("Licensed under Creative Commons: By Attribution 4.0 License"),
				  std::string("http://creativecommons.org/licenses/by/4.0/"),
				  std::string(" "),
				  std::string("\"Holiday Weasel\" by Kevin MacLeod"),
				  std::string("https://incompetech.filmmusic.io/song/5517-holiday-weasel"),
				  std::string("Licensed under Creative Commons: By Attribution 4.0 License"),
				  std::string("http://creativecommons.org/licenses/by/4.0/"),
				  std::string(" "),
				  std::string(" "),
				  std::string("versione 1.0.1"),
				  std::string("Copyright 2020 Massimiliano Maniscalco"),
				  std::string("questo è un software libero (GPL vers. 3)"),
				  std::string(" ") };
  std::vector<Text*> vec3;
  std::vector<std::string>::const_iterator it = vec2.cbegin();
  while (it != vec2.cend()) {
    try {
      Text* lbl = new Text(renderer, *it, title_font, COL_TXT_GAME);
      vec3.push_back(lbl);
    } catch (const Text::TextError& te) {
      std::cerr << "SDLEngine::init_title " << te.get_message() << std::endl;
    }
    it++;
  }
  titlescroll.set_text(vec3, TextScroll::Alignment::centre);
  
  // load books texture images
  SDL_Texture* txtr;
  std::string path { main_path + GRAPHICS_PATH + std::string("books.png") };
  if ((txtr = IMG_LoadTexture(renderer, path.c_str())) == nullptr) {
    //throw SpriteError { std::string { "IMG_LoadTexture error: " } + SDL_GetError() };
    std::cerr << "load texture error - " << path << std::endl;

  }
  textures.insert(std::pair<int,SDL_Texture*>(TXTR_BOOKS, txtr));

  // create sprite books
  try {
    Sprite* sprt = new Sprite(renderer, textures.at(TXTR_BOOKS));
    sprt->set_source_rect( { 0, 0, 512, 512 } );
    sprt->set_position( { (win_width - 512) / 2, win_height / 3, 512, 512 } );
    sprt->set_current_frame(0);
    sprt->set_total_frames(1);
    sprites.insert(std::pair<char, Sprite*>(SPRITE_BOOKS, sprt));
  } catch (const Sprite::SpriteError& se) {
    std::cerr << "SDLEngine::init_game() " << se.get_message() << std::endl;
  } catch (const std::out_of_range& oor) {
    std::cerr << "SDLEngine::init_game() couldn't find TXTR_BOOKS" << std::endl;
  }
}

void SDLEngine::init_save_slots_menu(void) {
  label_index = 1;
  try {
    Text* lbl = new Text(renderer, "Scegli un file di salvataggio", title_font, COL_TXT_GAME);
    SDL_Rect dest = lbl->get_position();
    dest.x = (win_width - dest.w) / 2;
    dest.y = win_height * 0.10;
    lbl->set_position(&dest);
    labels.push_back(lbl);
  } catch (const Text::TextError& te) {
    std::cerr << "SDLEngine::init_loading " << te.get_message() << std::endl;
  }

  for (short i = 1; i < 6; i++) {
    std::stringstream ss;
    ss << "slot" << i << ".save";
    struct stat buffer;
    std::string path { save_path + ss.str() };
    int result = stat(path.c_str(),&buffer);
    ss.str("");
    ss << "<slot " << i << ">";
    if (result == 0) {
      ss << " - saved game";      
    } else {
      ss << " - empty     ";
    }
    try {
      Text* lbl = new Text(renderer, ss.str(), title_font, COL_TXT_GAME);
      SDL_Rect dest = lbl->get_position();
      dest.x = (win_width - dest.w) / 2;
      dest.y = win_height * (0.15 + (0.05 * i));
      lbl->set_position(&dest);
      labels.push_back(lbl);
    } catch (const Text::TextError& te) {
      std::cerr << "SDLEngine::init_loading " << te.get_message() << std::endl;
    }
  }

  try {
    labels.at(label_index)->set_fading_mode(Fading::Mode::in_out_loop);
  } catch (const std::out_of_range& oor) {
    std::cerr << "SDLEngine::init_loading() out of range error" << std::endl;
  }
}

void SDLEngine::init_starting(void) {
  const std::vector<std::string>* vec { gdata->get_titles() };
  std::vector<std::string>::const_iterator iter = vec->cbegin();
  short i = 1;
  label_index = 0;
  while (iter != vec->cend()) {
    try {
      SDL_Colour col;
      if (player.story_is_unlocked(i - 1)) {
	col = COL_TXT_GAME;
      } else {
	col = COL_TXT_DISABLED;
      }
      Text* lbl = new Text(renderer, *iter, title_font, col);
      SDL_Rect dest = lbl->get_position();
      dest.x = (win_width - dest.w) / 2;
      dest.y = win_height * (0.15 + (0.05 * i));
      lbl->set_position(&dest);
      labels.push_back(lbl);
    } catch (const Text::TextError& te) {
      std::cerr << "SDLEngine::init_starting " << te.get_message() << std::endl;
    }
    i++;
    iter++;
  }
  if (labels.size() > 0) {
    labels.at(label_index)->set_fading_mode(Fading::Mode::in_out_loop);
  }

  try {
    Sketch* sktc = new Sketch { renderer, { 0, 0, 200, 200 } };
    SDL_Colour col1 { 255, 0, 0, 255 };
    SDL_Colour col2 { 255, 255, 0, 255 };
    sktc->add_drawings(Sketch::create_flower(0, 0, 200, col1, col2));
    int x = win_width / 1.30;
    int y = win_height / 6;
    sktc->set_position({ x, y, 200, 200 });
    for (int i = 1; i < 26; i++) {
      double a = PI / 2.0 / 25.0 * i;
      double t = 100;
      sktc->add_transformation();
      sktc->apply_translation(-t, -t, i);
      sktc->apply_rotate(a, i);
      sktc->apply_translation(t, t, i);
    }
    sktc->set_animation_mode(Animation::Mode::loop1way);
    sktc->set_alpha(255);
    sketches.push_back(sktc);
  } catch (const Sketch::SketchError& se) {
    std::cerr << "SDLEngine::add_random_sketches() " << se.get_message() << std::endl;
  }
}

void SDLEngine::init_game(void) {
  gamedraw = new Drawing();
  gamedraw->set_renderer(renderer);
  gamedraw->set_width(win_width);
  gamedraw->set_height(win_height);
  gamedraw->set_number_patterns(8);
  std::vector<std::pair<int, SDL_Colour>> pattern = { { 2, { 150, 0, 0, 50 } }, { 6, { 150, 150, 0, 50 } }, { 14, { 0, 150, 0, 50 } }, { 26, { 0, 150, 150, 50 } },
                                                      { 42, { 0, 0, 150, 50 } }, { 62, { 150, 0, 150, 50 } }, { 68, { 150, 0, 0, 50 } }, { 88, { 150, 150, 0, 50 } },
						      { 104, { 0, 150, 0, 50 } }, { 116, { 0, 150, 150, 50 } }, { 122, { 0, 0, 150, 50 } }, { 126, { 150, 0, 150, 50 } } };
  gamedraw->set_pattern(pattern);
  gamedraw->set_background_colour({ 220, 220, 220, 255 });
  gamedraw->create_pattern();
  sketches_last = std::chrono::system_clock::time_point();
  add_random_sketches(SketchesSchema::waterfall);

  // load big coin texture images
  SDL_Texture* txtr;
  std::string path { main_path + GRAPHICS_PATH + std::string("gold_coin_sprite.png") };
  if ((txtr = IMG_LoadTexture(renderer, path.c_str())) == nullptr) {
    //throw SpriteError { std::string { "IMG_LoadTexture error: " } + SDL_GetError() };
    std::cerr << "load texture error - " << path << std::endl;
  }
  textures.insert(std::pair<int,SDL_Texture*>(TXTR_BIG_COIN, txtr));

  // load speaker texture images
  path.assign(main_path + GRAPHICS_PATH + std::string("speaker_icon.png"));
  if ((txtr = IMG_LoadTexture(renderer, path.c_str())) == nullptr) {
    //throw SpriteError { std::string { "IMG_LoadTexture error: " } + SDL_GetError() };
    std::cerr << "load texture error - " << path << std::endl;
  }
  textures.insert(std::pair<int,SDL_Texture*>(TXTR_SPEAKER, txtr));

  // load small coin texture images
  path.assign(main_path + GRAPHICS_PATH + std::string("small_coin_sprite.png"));
  if ((txtr = IMG_LoadTexture(renderer, path.c_str())) == nullptr) {
    //throw SpriteError { std::string { "IMG_LoadTexture error: " } + SDL_GetError() };
    std::cerr << "load texture error - " << path << std::endl;
  }
  textures.insert(std::pair<int,SDL_Texture*>(TXTR_SMALL_COIN, txtr));

  // create sprite coins
  try {
    Sprite* sprt = new Sprite(renderer, textures.at(TXTR_BIG_COIN));
    sprt->set_source_rect( { 0, 0, 75, 75 } );
    sprt->set_position( { 10, 10, 75, 75 } );
    sprt->set_current_frame(0);
    sprt->set_total_frames(10);
    sprt->set_animation_mode(Animation::Mode::none);
    sprites.insert(std::pair<char, Sprite*>(SPRITE_COINS, sprt));
  } catch (const Sprite::SpriteError& se) {
    std::cerr << "SDLEngine::init_game() " << se.get_message() << std::endl;
  } catch (const std::out_of_range& oor) {
    std::cerr << "SDLEngine::init_game() couldn't find TXTR_BIG_COIN" << std::endl;
  }

  // create sprite speaker
  try {
    Sprite* sprt = new Sprite(renderer, textures.at(TXTR_SPEAKER));
    sprt->set_source_rect( { 0, 0, 80, 85 } );
    sprt->set_position( { win_width - 100, static_cast<int>(win_height / 15.0), 80, 85 } );
    sprt->set_current_frame(0);
    sprt->set_total_frames(3);
    sprt->set_animation_mode(Animation::Mode::none);
    sprt->set_sensitive(true);
    sprt->set_speed({ 0.0, 2.0});
    sprites.insert(std::pair<char, Sprite*>(SPRITE_SPEAKER, sprt));
  } catch (const Sprite::SpriteError& se) {
    std::cerr << "SDLEngine::init_game() " << se.get_message() << std::endl;
  } catch (const std::out_of_range& oor) {
    std::cerr << "SDLEngine::init_game() couldn't find TXTR_SPEAKER" << std::endl;
  }

  // create Text coins number
  std::stringstream ss;
  ss << player.get_coins();
  try {
    Text* lbl = new Text(renderer, ss.str(), big_font, COL_TXT_COINS);
    SDL_Rect dest = lbl->get_position();
    dest.x = 100;
    dest.y = 5;
    lbl->set_position(&dest);
    map_labels.insert(std::pair<char,Text*>(LABEL_COINS,lbl));
  } catch (const Text::TextError& te) {
    std::cerr << "SDLEngine::init_game " << te.get_message() << std::endl;
  }

  storyscroll.set_rect({ win_width / 15, win_height / 8, win_width, win_height });
  storyscroll.set_max_rows(20);
  storyscroll.clear_rows();
  add_sentence();

  // create Text current label
  try {
    Text* lbl = new Text(renderer, std::string(" "), story_font, COL_TXT_GAME);
    lbl->set_speed({ 0.0, 2.0 });
    map_labels.insert(std::pair<char,Text*>(LABEL_CURRENT,lbl));
  } catch (const Text::TextError& te) {
    std::cerr << "SDLEngine::init_game " << te.get_message() << std::endl;
  }
  
  set_current_label_position();
  update_speaker_position();
  
  // load coin earned sound
  path.assign(main_path + SOUNDS_PATH + "coin_earned.wav");
  Mix_Chunk* sound = Mix_LoadWAV(path.c_str());
  if (!sound) {
    on_SDL_error(std::cerr, std::string("Loud sound ") + path);
  } else {
    Mix_VolumeChunk(sound, 64);
    sounds.insert(std::pair<char,Mix_Chunk*>(SOUND_COIN_EARNED,sound));
  }

  // load kids yeah sound
  path.assign(main_path + SOUNDS_PATH + "kids_yeah.wav");
  sound = Mix_LoadWAV(path.c_str());
  if (!sound) {
    on_SDL_error(std::cerr, std::string("Loud sound ") + path);
  } else {
    Mix_VolumeChunk(sound, 32);
    sounds.insert(std::pair<char,Mix_Chunk*>(SOUND_KIDS_YEAH,sound));
  }

  // load kid no sound
  path.assign(main_path + SOUNDS_PATH + "kid_no.wav");
  sound = Mix_LoadWAV(path.c_str());
  if (!sound) {
    on_SDL_error(std::cerr, std::string("Loud sound ") + path);
  } else {
    Mix_VolumeChunk(sound, 32);
    sounds.insert(std::pair<char,Mix_Chunk*>(SOUND_KID_NO,sound));
  }

  Mix_FadeOutMusic(750);
  current_music = GAME_MUSIC;
  music_volume = GAME_MUSIC_VOLUME;

  SDL_StartTextInput();
  update_current_label();
}

void SDLEngine::init_report(void) {
  int story_size = gdata->get_story_size();
  if (gamestatus == GameStatus::endstory) {
    double ratio = (player.get_coins() * 100.0) / story_size;
    std::string praise;
    if (ratio >= 98.0) {
      praise.assign("Super!!!");
    } else if (ratio >= 90.0 && ratio < 98.0) {
      praise.assign("Sei un campione!!!");
    } else if (ratio >= 80.0 && ratio < 90.0) {
      praise.assign("Bravissimo!!!");
    } else if (ratio >= 70.0 && ratio < 80.0) {
      praise.assign("Bravo!");
    } else if (ratio >= 50.0 && ratio < 70.0) {
      praise.assign("Bene, ma puoi fare di meglio!");
    } else if (ratio >= 30.0 && ratio < 50.0) {
      praise.assign("Coraggio, ascolta con attenzione prima di scrivere!");
    } else if (ratio < 30.0) {
      praise.assign("Mettici più impegno!");
    }
    try {
      Text* lbl = new Text(renderer, praise, big_font, COL_TXT_GAME);
      SDL_Rect dest = lbl->get_position();
      dest.x = (win_width - dest.w) / 2;
      dest.y = dest.h * 1.5;
      lbl->set_position(&dest);
      lbl->set_alpha(0);
      lbl->set_fading_mode(Fading::Mode::in);
      lbl->set_fading_pace(2);
      labels.push_back(lbl);
    } catch (const Text::TextError& te) {
      std::cerr << "SDLEngine::init_report() " << te.get_message() << std::endl;
    }
  }
  try {
    std::stringstream stats;
    int answers = gdata->get_current_sentence_index();
    int corrects = player.get_current_correct();
    if (answers == 0) {
      stats << "Nessuna frase completata";
    } else {
      if (corrects == 0) {
	stats << "Nessuna parola corretta su ";
      } else if (corrects == 1) {
	stats << corrects << " parola corretta su ";
      } else {
	stats << corrects << " parole corrette su ";
      }
      if (answers == 1) {
	stats << answers << " frase completata";
      } else {
	stats << answers << " frasi completate";
      }
    }
    if (story_size - answers == 1) {
      stats << ", 1 frase non completata";
    } else if (story_size - answers > 1) {
      stats << ", " << story_size - answers << " frasi da completare";
    }
    Text* lbl = new Text(renderer, stats.str(), story_font, COL_TXT_GAME);
    SDL_Rect pos = lbl->get_position();
    pos.x = (win_width - pos.w) / 2;
    pos.y = (win_height - pos.h) / 3;
    lbl->set_position(&pos);
    lbl->set_alpha(0);
    lbl->set_fading_mode(Fading::Mode::in);
    lbl->set_fading_pace(2);
    labels.push_back(lbl);
  } catch (const Text::TextError& te) {
    std::cerr << "SDLEngine::init_report() " << te.get_message() << std::endl;
  }
  short i = 0;
  std::vector<std::string> menu { "salva i risultati", "non salvare" };
  std::vector<std::string>::const_iterator iter = menu.cbegin();
  while (iter != menu.cend()) {
    try {
      Text* lbl = new Text(renderer, *iter, title_font, COL_TXT_GAME);
      SDL_Rect pos = lbl->get_position();
      pos.x = (win_width - pos.w) / 2;
      pos.y = ((win_height - pos.h) / 1.5) + pos.h * i ;
      lbl->set_position(&pos);
      labels.push_back(lbl);
    } catch (const Text::TextError& te) {
      std::cerr << "SDLEngine::init_report() " << te.get_message() << std::endl;
    }
    i++;
    iter++;
  }
  label_index = labels.size() == 4 ? 2 : 1;
  if (labels.size() > 0) {
    labels.at(label_index)->set_fading_mode(Fading::Mode::in_out_loop);
  }
  if (player.get_coins() == gdata->get_current_sentence_index() + 1) {
    if (!player.story_is_unlocked(gdata->get_current_story_index() + 1)) {
      player.unlock_story(); // unlock next story
      try {
	Text* lbl = new Text(renderer, std::string("Hai sbloccato una nuova storia!"), big_font, COL_TXT_RIGHT);
	SDL_Rect dest = lbl->get_position();
	dest.x = (win_width - dest.w) / 2;
	dest.y = win_height - (dest.h * 1.5);
	lbl->set_position(&dest);
	lbl->set_alpha(0);
	lbl->set_fading_mode(Fading::Mode::in_out);
	lbl->set_fading_pace(2);
	map_labels.insert(std::pair<char,Text*>({ LABEL_MESSAGE, lbl }));
      } catch (const Text::TextError& te) {
	std::cerr << "SDLEngine::init_report() " << te.get_message() << std::endl;
      }
    }
  }
  sketches_last = std::chrono::system_clock::time_point();
  add_random_sketches(SketchesSchema::fireworks);

  Mix_FadeOutMusic(750);
  current_music = TITLE_MUSIC;
  music_volume = TITLE_MUSIC_VOLUME;
}

void SDLEngine::on_title_event(SDL_Event* event) {
  if (event->type == SDL_KEYDOWN) {
    if (event->key.keysym.sym == SDLK_ESCAPE) {
      while (!Mix_FadeOutMusic(500) && Mix_PlayingMusic()) {
	SDL_Delay(100);
      }
      gamestatus = GameStatus::quit;
    } else if (event->key.keysym.sym == SDLK_UP) {
      update_fading_label(label_index, Fading::Mode::none);
      label_index--;
      if (label_index < 0) {
	label_index = labels.size() - 1;
      }
      update_fading_label(label_index, Fading::Mode::in_out_loop);
    } else if (event->key.keysym.sym == SDLK_DOWN) {
      update_fading_label(label_index, Fading::Mode::none);
      label_index++;
      if (label_index == labels.size()) {
	label_index = 0;
      }
      update_fading_label(label_index, Fading::Mode::in_out_loop);
    } else if (event->key.keysym.sym == SDLK_RETURN) {
      free_memory();
      switch (label_index) {
      case 0:
	gamestatus = GameStatus::starting;
	init_starting();
	break;
      case 1:
	gamestatus = GameStatus::loading;
	init_save_slots_menu();
	break;
      }
    }
  } 
}

void SDLEngine::on_loading_event(SDL_Event* event) {
  if (event->type == SDL_KEYDOWN) {
    if (event->key.keysym.sym == SDLK_ESCAPE) {
      gamestatus = GameStatus::title;
      free_memory();
      init_title();
    } else if (event->key.keysym.sym == SDLK_UP) {
      update_fading_label(label_index, Fading::Mode::none);
      label_index--;
      if (label_index < 1) {
	label_index = labels.size() - 1;
      }
      update_fading_label(label_index, Fading::Mode::in_out_loop);
    } else if (event->key.keysym.sym == SDLK_DOWN) {
      update_fading_label(label_index, Fading::Mode::none);
      label_index++;
      if (label_index == labels.size()) {
	label_index = 1;
      }
      update_fading_label(label_index, Fading::Mode::in_out_loop);
    } else if (event->key.keysym.sym == SDLK_RETURN) {
      if (check_slot(label_index)) {
	if (load_game(label_index)) {
	  if (gdata->get_current_sentence_index() == gdata->get_story_size() - 1) {
	    gamestatus = GameStatus::starting;
	    free_memory();
	    init_starting();
	  } else {
	    gamestatus = GameStatus::game;
	    free_memory();
	    init_game();
	  }
	} else {
	  std::cerr << "load game failed!" << std::endl;
	}
      }
    }
  }
}

void SDLEngine::on_starting_event(SDL_Event* event) {
  if (event->type == SDL_KEYDOWN) {
    if (event->key.keysym.sym == SDLK_ESCAPE) {
      gamestatus = GameStatus::title;
      free_memory();
      init_title();
    } else if (event->key.keysym.sym == SDLK_UP) {
      update_fading_label(label_index, Fading::Mode::none);
      label_index--;
      if (label_index < 0) {
	label_index = labels.size() - 1;
      }
      update_fading_label(label_index, Fading::Mode::in_out_loop);
    } else if (event->key.keysym.sym == SDLK_DOWN) {
      update_fading_label(label_index, Fading::Mode::none);
      label_index++;
      if (label_index == labels.size()) {
	label_index = 0;
      }
      update_fading_label(label_index, Fading::Mode::in_out_loop);
    } else if (event->key.keysym.sym == SDLK_RETURN) {
      if (player.story_is_unlocked(label_index)) {
	gdata->set_current_story_index(label_index);
	gdata->load_story();
	if (!player.check_story(label_index)) {
	  player.add_story();
	}
	player.set_current_story_index(label_index);
	gamestatus = GameStatus::game;
	free_memory();
	init_game();
      }
    }
  }
}

void SDLEngine::on_game_event(SDL_Event* event) {
  if (event->type == SDL_TEXTINPUT) {
    std::string chr = event->text.text;
    keybuffer.append(chr);
    update_current_label();
  } else if (event->type == SDL_KEYDOWN) {
    if (event->key.keysym.sym == SDLK_ESCAPE) {
      SDL_StopTextInput();
      gamestatus = GameStatus::breakstory;
      free_memory();
      init_report();
    } else if (event->key.keysym.sym == SDLK_RETURN) {
      if (storyscroll.is_ready()) {
	SDL_StopTextInput();
	check_word(keybuffer);
	keybuffer.assign(std::string());
	if (gdata->next_sentence()) {
	  if (add_sentence()) {
	    update_current_label();
	    if (!storyscroll.is_over_max_rows()) {
	      update_speaker_position();
	    }
	    SDL_StartTextInput();
	  }
	} else {
	  gamestatus = GameStatus::endstory;
	  free_memory();
	  init_report();
	}
      }
    } else if (event->key.keysym.sym == SDLK_BACKSPACE) {
      if (keybuffer.size() > 0) {
	keybuffer.assign(keybuffer.substr(0,keybuffer.size() - 1));
	update_current_label();
      }
    }
  } else if (event->type == SDL_MOUSEBUTTONDOWN) {
    check_sprites_click(event);
  }
}

void SDLEngine::on_report_event(SDL_Event* event) {
  if (event->type == SDL_KEYDOWN) {
    int min_index = labels.size() == 4 ? 2 : 1;
    if (event->key.keysym.sym == SDLK_ESCAPE) {
      gamestatus = GameStatus::title;
      free_memory();
      init_title();
    } else if (event->key.keysym.sym == SDLK_UP) {
      update_fading_label(label_index, Fading::Mode::none);
      label_index--;
      if (label_index < min_index) {
	label_index = labels.size() - 1;
      }
      update_fading_label(label_index, Fading::Mode::in_out_loop);
    } else if (event->key.keysym.sym == SDLK_DOWN) {
      update_fading_label(label_index, Fading::Mode::none);
      label_index++;
      if (label_index == labels.size()) {
	label_index = min_index;
      }
      update_fading_label(label_index, Fading::Mode::in_out_loop);
    } else if (event->key.keysym.sym == SDLK_RETURN) {
      if (label_index == min_index) {
	gamestatus = GameStatus::saving;
	free_memory();
	init_save_slots_menu();
      } else {
	free_memory();
	if (gamestatus == GameStatus::endstory) {
	  gamestatus = GameStatus::starting;
	  init_starting();
	} else {
	  gamestatus = GameStatus::title;
	  init_title();
	}
      }
    }
  }
}

void SDLEngine::on_saving_event(SDL_Event* event) {
  if (event->type == SDL_KEYDOWN) {
    if (event->key.keysym.sym == SDLK_ESCAPE) {
      gamestatus = GameStatus::starting;
      free_memory();
      init_starting();
    } else if (event->key.keysym.sym == SDLK_UP) {
      update_fading_label(label_index, Fading::Mode::none);
      label_index--;
      if (label_index < 1) {
	label_index = labels.size() - 1;
      }
      update_fading_label(label_index, Fading::Mode::in_out_loop);
    } else if (event->key.keysym.sym == SDLK_DOWN) {
      update_fading_label(label_index, Fading::Mode::none);
      label_index++;
      if (label_index == labels.size()) {
	label_index = 1;
      }
      update_fading_label(label_index, Fading::Mode::in_out_loop);
    } else if (event->key.keysym.sym == SDLK_RETURN) {
      save_game(label_index);
      gamestatus = GameStatus::starting;
      free_memory();
      init_starting();
    }
  }
}

void SDLEngine::title_render(void) {
  SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_BLEND);
  SDL_Colour col = { 255, 255, 255, 255 };
  SDL_SetRenderDrawColor(renderer,col.r,col.g,col.b,col.a);
  SDL_RenderClear(renderer);
  titlescroll.draw();
  draw_sprites();
  draw_labels();
  SDL_RenderPresent(renderer);
}

void SDLEngine::loading_render(void) {
  SDL_SetRenderDrawColor(renderer,255,255,255,255);
  SDL_RenderClear(renderer);
  draw_labels();
  SDL_RenderPresent(renderer);
}

void SDLEngine::starting_render(void) {
  SDL_SetRenderDrawColor(renderer,255,255,255,255);
  SDL_RenderClear(renderer);
  draw_labels();
  if (map_labels.count(LABEL_MESSAGE) > 0) {
    map_labels.at(LABEL_MESSAGE)->draw();
  }
  draw_sketches();
  SDL_RenderPresent(renderer);
}

void SDLEngine::game_render(void) {
  gamedraw->draw();
  draw_sketches();
  storyscroll.draw();
  draw_labels();
  draw_sprites();
  std::vector<Sprite*>::iterator itt = temp_sprites.begin();
  while (itt != temp_sprites.end()) {
    if (*itt) {
      (*itt)->draw();
    }
    itt++;
  }
  
  SDL_RenderPresent(renderer);
}

void SDLEngine::report_render(void) {
  SDL_SetRenderDrawColor(renderer,255,255,255,255);
  SDL_RenderClear(renderer);
  draw_labels();
  draw_sketches();
  SDL_RenderPresent(renderer);
}

void SDLEngine::saving_render(void) {
  SDL_SetRenderDrawColor(renderer,255,255,255,255);
  SDL_RenderClear(renderer);
  draw_labels();
  SDL_RenderPresent(renderer);
}

void SDLEngine::check_word(const std::string& str) {
  bool result = gdata->check_current_word(str);
  clone_current_label(result);
  if (result) {
    try {
      Mix_PlayChannel(CHANNEL_KIDS,sounds.at(SOUND_KIDS_YEAH),0);
    } catch (const std::out_of_range& oor) {
      std::cerr << "Error: missing SOUND_KIDS_YEAH" << std::endl;
    }
    if (!player.check_result(gdata->get_current_sentence_index())) {
      player.add_coin();
      update_coins();
      add_small_coin();
    }
  } else {
    std::string str { gdata->get_current_word() };
    if (str.compare(std::string("")) == 0) {
      str.assign(std::string(" "));
    }
    try {
      Text* lbl = new Text(renderer, str, story_font, COL_TXT_WRONG);
      SDL_Rect orig = lbl->get_position();
      SDL_Rect pos = map_labels.at(LABEL_CURRENT)->get_position();
      pos.w = orig.w;
      pos.h = orig.h;
      lbl->set_position(&pos);
      lbl->set_speed({ 0.0, 2.0 });
      lbl->set_alpha(0);
      lbl->set_fading_mode(Fading::Mode::in);
      labels.push_back(lbl);
    } catch (const Text::TextError& te) {
      std::cerr << "SDLEngine::check_word() " << te.get_message() << std::endl;
    }
    try {
      Mix_PlayChannel(CHANNEL_KIDS,sounds.at(SOUND_KID_NO),0);
    } catch (const std::out_of_range& oor) {
      std::cerr << "Error: missing SOUND_KID_NO" << std::endl;
    }
  }
  player.save_result(gdata->get_current_sentence_index(), result);
}

void SDLEngine::check_sprites_click(SDL_Event* event) {
  SDL_Point p { event->button.x, event->button.y };
  if (sprites.at(SPRITE_SPEAKER)->check_position(&p)) {
    try {
      music_volume = GAME_MUSIC_LOW_VOLUME;
      Mix_VolumeMusic(music_volume);
      Mix_PlayChannel(CHANNEL_SPELL,sounds.at(SOUND_SPELL),0);
      sprites.at(SPRITE_SPEAKER)->set_animation_reps(2);
      sprites.at(SPRITE_SPEAKER)->set_animation_mode(Animation::Mode::twoway);
    } catch (const std::out_of_range& oor) {
      std::cerr << "SDLEngine::check_sprites_click() out of range error" << std::endl;
    }
  }
}

/* static function call back when channel spell stopped playing */
void SDLEngine::channel_spell_done(int chn) {
  if (chn == CHANNEL_SPELL) {
    music_volume = GAME_MUSIC_VOLUME;
    Mix_VolumeMusic(music_volume);
  }
}

/* static function call back when music stopped playing */
void SDLEngine::music_done(void) {
  if (Mix_FadeInMusic(musics.at(current_music), -1, 750) != 0) {
    on_SDL_error(std::cerr, "Play music");
  }
}

bool SDLEngine::load_game(short slot) {
  std::stringstream ss;
  ss << "slot" << slot << ".save";
  std::string path { save_path + ss.str() };
  int csi;
  bool result = player.load(path, &csi);
  if (result) {
    gdata->set_current_story_index(player.get_current_story_index());
    gdata->load_story();
    gdata->set_current_sentence_index(csi);
  }
  return result;
}

bool SDLEngine::save_game(short slot) {
  std::stringstream ss;
  ss << "slot" << slot << ".save";
  std::string path { save_path + ss.str() };
  return player.save(path, gdata->get_current_sentence_index());
}

bool SDLEngine::check_slot(short slot) {
  std::stringstream ss;
  ss << "slot" << slot << ".save";
  std::string slot_path { save_path + ss.str() };
  struct stat buffer;
  int result = stat(slot_path.c_str(),&buffer);
  return result == 0;
}

bool SDLEngine::add_sentence(void) {
  std::string sentence { gdata->get_current_sentence() };
  if (sentence.compare(std::string()) != 0) {
    try {
      Text* lbl = new Text(renderer, sentence, story_font, COL_TXT_GAME);
      storyscroll.add_row(lbl);
      if (storyscroll.is_over_max_rows()) {
	TextScroll::fade_out(&labels, 1);
	TextScroll::scroll(&labels);
      }
    } catch (const Text::TextError& te) {
      std::cerr << "SDLEngine::add_sentence " << te.get_message() << std::endl;
    }
    free_sound_spell();
    std::string path { main_path + SPELLS_PATH + gdata->get_current_spell_path() };
    Mix_Chunk* s = Mix_LoadWAV(path.c_str());
    if (!s) {
      on_SDL_error(std::cerr, std::string("Loud sound ") + path);
    } else {
      Mix_VolumeChunk(s, 128);
      sounds.insert(std::pair<char,Mix_Chunk*>(SOUND_SPELL,s));
    }
    return true;
  }
  return false;
}

void SDLEngine::set_current_label_position(void) {
  int w = 0;
  std::string str = gdata->get_first_half_sentence();
  if (str.compare(std::string()) != 0) {
    SDL_Surface *surf = TTF_RenderUTF8_Blended(story_font, str.c_str(), COL_TXT_GAME);
    if (surf == nullptr) {
      std::cerr << "SDLEngine::set_current_label_position() TTF_RenderUTF8_Blended error: " << SDL_GetError() << std::endl;
    } else {
      w = surf->w;
      SDL_FreeSurface(surf);
    }
  }
  try {
    Text* txt = map_labels.at(LABEL_CURRENT);
    SDL_Rect dest = txt->get_position();
    dest.x = (win_width / 15.0) + w;
    dest.y = storyscroll.get_last_row()->get_position().y;
    txt->set_position(&dest);
    if (storyscroll.is_over_max_rows()) {
      txt->add_destination({ dest.x, dest.y - dest.h });
    }
  } catch (const Text::TextError& te) {
    std::cerr << "SDLEngine::set_current_label_position " << te.get_message() << std::endl;
  }
}

void SDLEngine::update_current_label(void) {
  std::string str { keybuffer };
  try {
    map_labels.at(LABEL_CURRENT)->rebuild_texture(str + std::string("|"),COL_TXT_GAME);
  } catch (const Text::TextError& te) {
    std::cerr << "SDLEngine::update_current_label " << te.get_message() << std::endl;
  } catch (const std::out_of_range& oor) {
    std::cerr << "SDLEngine::update_current_label out of range in std::map" << std::endl;
  }
  set_current_label_position();
}

void SDLEngine::clone_current_label(bool correct) {
  std::string str { keybuffer };
  if (str.compare(std::string()) == 0) {
    //str.assign(std::string(" "));
    return;
  }
  try {
    SDL_Colour col = correct ? SDL_Colour COL_TXT_RIGHT : SDL_Colour COL_TXT_WRONG;
    Text* lbl = new Text(renderer, str, story_font, col);
    SDL_Rect pos = map_labels.at(LABEL_CURRENT)->get_position();
    lbl->set_position(&pos);
    lbl->set_speed({ 0.0, 2.0 });
    if (!correct) {
      lbl->add_destination({ pos.x, pos.y - (pos.h * 2) });
      lbl->set_fading_mode(Fading::Mode::out);
      lbl->set_fading_pace(0.0);
      lbl->set_fading_acceleration(0.1);
    }
    labels.push_back(lbl);
  } catch (const Text::TextError& te) {
    std::cerr << "SDLEngine::clone_current_label " << te.get_message() << std::endl;
  }
}

void SDLEngine::update_speaker_position(void) {
  try {
    Sprite* sprt = sprites.at(SPRITE_SPEAKER);
    SDL_Rect sprt_pos = sprt->get_position();
    SDL_Rect lbl_pos = storyscroll.get_last_row()->get_position();
    sprt_pos.y = lbl_pos.y - ((sprt_pos.h - lbl_pos.h) / 2);
    sprt->add_destination({ sprt_pos.x, sprt_pos.y});
  } catch (const std::out_of_range& oor) {
    std::cerr << "Error while updating speaker position" << std::endl;
  }
}

void SDLEngine::add_small_coin(void) {
  try {
    Sprite* sprt = new Sprite(renderer, textures.at(TXTR_SMALL_COIN));
    sprt->set_source_rect( { 0, 0, 30, 30 } );
    SDL_Rect sprt_pos { 0, 0, 30, 30 };
    SDL_Rect lbl_pos = map_labels.at(LABEL_CURRENT)->get_position();
    sprt_pos.x = lbl_pos.x + (lbl_pos.w / 2);
    sprt_pos.y = lbl_pos.y - ((sprt_pos.h - lbl_pos.h) / 2);
    sprt->set_position(sprt_pos);
    sprt->set_current_frame(0);
    sprt->set_total_frames(10);
    sprt->set_animation_mode(Animation::Mode::loop1way);
    sprt->set_visible(true);
    sprt->add_destination({sprt_pos.x, sprt_pos.y - 70});
    sprt->set_speed({0.0,-1.0});
    sprt->set_alpha(255);
    sprt->set_fading_mode(Fading::Mode::out);
    sprt->set_fading_pace(0.0);
    sprt->set_fading_acceleration(0.1);
    temp_sprites.push_back(sprt);
  } catch (const Sprite::SpriteError& se) {
    std::cerr << "SDLEngine::put_small_coin() " << se.get_message() << std::endl;
  } catch (const std::out_of_range& oor) {
    std::cerr << "SDLEngine::put_small_coin() out of range in std::map" << std::endl;
  }
}

void SDLEngine::update_fading_label(int index, Fading::Mode mode) {
  try {
    labels.at(index)->set_fading_mode(mode);
    labels.at(index)->set_alpha(255);
  } catch (const std::out_of_range& oor) {
    std::cerr << "SDLEngine::update_fading_label() out of range error" << std::endl;
  }
}

void SDLEngine::add_random_sketches(SketchesSchema mode) {
  std::chrono::high_resolution_clock::time_point sktc_now = std::chrono::high_resolution_clock::now();
  double ddtp = std::chrono::duration_cast<std::chrono::milliseconds>(sktc_now - sketches_last).count();
  if (ddtp > sketches_delay) {
    sketches_last = sktc_now;
    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    std::default_random_engine generator(seed);
    std::uniform_int_distribution<int> numb_distr(1, 4);
    std::uniform_int_distribution<int> side_distr(20, 200);
    std::uniform_int_distribution<Uint8> col_distr(0, 5);
    std::uniform_int_distribution<int> image_distr(0, 1);
    std::vector<SDL_Colour> colours { { 255,0,0,255 }, { 255,255,0,255 }, { 0,255,0,255 }, { 0,255,255,255 }, { 0,0,255,255 }, { 255,0,255,255 } };
    int numb = numb_distr(generator);
    for (int i = 0; i < numb; i++) {
      int side = side_distr(generator);
      try {
	Sketch* sktc = new Sketch { renderer, { 0, 0, side, side } };
	SDL_Colour col1 { colours.at(col_distr(generator)) };
	SDL_Colour col2 { colours.at(col_distr(generator)) };
	while (col1.r == col2.r && col1.g == col2.g && col1.b == col2.b) {
	  col2 = colours.at(col_distr(generator));
	}
	int image = image_distr(generator);
	if (image == 0) {
	  sktc->add_drawing(Sketch::create_star(0, 0, side, col1));
	  sktc->add_drawing(Sketch::create_star(side / 4, side / 4, side / 2, col2));
	} else {
	  sktc->add_drawings(Sketch::create_flower(0, 0, side, col1, col2));
	}
	sktc->set_position({ win_width, 0, side, side });
	for (int i = 1; i < 26; i++) {
	  double a = image == 0 ? PI / 2.5 / 25.0 * i : PI / 2.0 / 25.0 * i;
	  double t = side / 2.0;
	  sktc->add_transformation();
	  sktc->apply_translation(-t, -t, i);
	  sktc->apply_rotate(a, i);
	  sktc->apply_translation(t, t, i);
	}
	sktc->set_animation_mode(Animation::Mode::loop1way);
	sktc->set_speed({ 2.0, 2.0 });
	sktc->set_alpha(60);
	switch (mode) {
	case SketchesSchema::waterfall:
	  set_waterfall_route(sktc);
	  break;
	case SketchesSchema::fireworks:
	  set_fireworks_route(sktc);
	  break;
	}
	sketches.push_back(sktc);
      } catch (const Sketch::SketchError& se) {
	std::cerr << "SDLEngine::add_random_sketches() " << se.get_message() << std::endl;
      }
    }
  }
}

void SDLEngine::set_waterfall_route(Sketch* sktc) {
  unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
  std::default_random_engine generator(seed);
  SDL_Rect pos = sktc->get_position();
  std::uniform_int_distribution<int> r_distr(win_height / 6, win_height / 2);
  std::uniform_int_distribution<int> x_distr(win_width / 8, win_width / 3);
  int r = r_distr(generator);
  pos.x = win_width;
  pos.y = r;
  sktc->set_position(pos);
  SDL_Point first { pos.x - x_distr(generator), pos.y + pos.h };
  sktc->add_destination(first);
  double sa = -PI / 2.0; // start angle
  double da = (-PI / 2.0) / 12.0; // delta angle
  double a = sa;
  for (int i = 0; i < 12; i++) {
    SDL_Point dest;
    dest.x = first.x + std::round(r * std::cos(a));
    dest.y = first.y + r + std::round(r * std::sin(a));
    sktc->add_destination(dest);
    a += da;
  }
  SDL_Point last;
  last.x = first.x + std::round(r * std::cos(a));
  last.y = win_height;
  sktc->add_destination(last);
}

void SDLEngine::set_fireworks_route(Sketch* sktc) {
  unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
  std::default_random_engine generator(seed);
  SDL_Rect pos = sktc->get_position();
  std::uniform_int_distribution<int> x_distr(0, 100);
  std::uniform_int_distribution<int> y_distr(win_height / 2.5, win_height / 1.25);
  std::uniform_int_distribution<int> r_distr(win_width / 6.0, win_width / 3.0);
  pos.x = (win_width - pos.w) / 2.0;
  pos.y = win_height;
  sktc->set_position(pos);
  int x1 = x_distr(generator) - 50;
  int y1 = y_distr(generator);
  SDL_Point first { pos.x + x1, y1 };
  sktc->add_destination(first);
  double sa = x1 >= 0 ? -PI : 0.0; // start angle
  double da = x1 >= 0 ? PI / 12.0 : -PI / 12.0; // delta angle
  double a = sa;
  int r = r_distr(generator);
  for (int i = 0; i < 12; i++) {
    SDL_Point dest;
    dest.x = x1 >= 0 ? first.x + r + std::round(r * std::cos(a)) : (first.x - r) + std::round(r * std::cos(a));
    dest.y = first.y + std::round(r * std::sin(a));
    sktc->add_destination(dest);
    if (dest.x < 0 || dest.y < 0) {
      break;
    }
    a += da;
  }
}

void SDLEngine::draw_sketches(void) {
  std::vector<Sketch*>::iterator iter = sketches.begin();
  while (iter != sketches.end()) {
    if (*iter) {
      (*iter)->draw();
    }
    iter++;
  }
}

void SDLEngine::update_sketches(SketchesSchema mode, bool destroy) {
  int n = 0;
  std::vector<Sketch*>::iterator iter = sketches.begin();
  while (iter != sketches.end()) {
    if (*iter) {
      (*iter)->animate();
      (*iter)->move();
      (*iter)->fade();
      if (destroy) {
	if ((*iter)->is_moving()) {
	  n++;
	  iter++;
	} else {
	  delete *iter;
	  *iter = nullptr;
	  iter = sketches.erase(iter);
	}
      } else {
	iter++;
      }
    }
  }
  if (destroy && n < 20) {
    add_random_sketches(mode);
  }
}

void SDLEngine::draw_labels(void) {
  std::vector<Text*>::iterator itl = labels.begin();
  while (itl != labels.end()) {
    if (*itl) {
      (*itl)->draw();
    }
    itl++;
  }
  std::map<char,Text*>::iterator itm = map_labels.begin();
  while (itm != map_labels.end()) {
    if (itm->second) {
      itm->second->draw();
    }
    itm++;
  }
}

void SDLEngine::update_labels(bool destroy) {
  std::vector<Text*>::iterator itl = labels.begin();
  while (itl != labels.end()) {
    if (*itl != nullptr) {
      (*itl)->move();
      (*itl)->fade();
      if (destroy && (*itl)->get_alpha() == 0) {
	delete *itl;
	*itl = nullptr;
	itl = labels.erase(itl);
      } else {
	itl++;
      }
    } else {
      itl++;
    }
  }
  std::map<char,Text*>::iterator itm = map_labels.begin();
  while (itm != map_labels.end()) {
    if (itm->second) {
      itm->second->fade();
    }
    itm++;
  }
}

void SDLEngine::draw_sprites(void) {
  std::map<char, Sprite*>::iterator its = sprites.begin();
  while (its != sprites.end()) {
    if (its->second) {
      its->second->draw();
    }
    its++;
  }
}

void SDLEngine::update_sprites(void) {
  std::map<char, Sprite*>::iterator its = sprites.begin();
  while (its != sprites.end()) {
    if (its->second) {
      its->second->animate();
      its->second->move();
      its->second->fade();
    }
    its++;
  }
  std::vector<Sprite*>::iterator itt = temp_sprites.begin();
  while (itt != temp_sprites.end()) {
    if (*itt) {
      (*itt)->animate();
      (*itt)->move();
      (*itt)->fade();
      if ((*itt)->get_alpha() == 0) {
	delete *itt;
	*itt = nullptr;
      }
      itt++;
    } else {
      itt = temp_sprites.erase(itt);
    }
  }
}

void SDLEngine::update_coins(void) {
  try {
    Text* lbl = map_labels.at(LABEL_COINS);
    std::stringstream ss;
    ss << player.get_coins();
    lbl->rebuild_texture(ss.str(), COL_TXT_COINS);
    SDL_Rect dest = lbl->get_position();
    dest.x = 100;
    dest.y = 5;
    lbl->set_position(&dest);
  } catch (const Text::TextError& te) {
    std::cerr << "SDLEngine::update_coins() " << te.get_message() << std::endl;
  } catch (const std::out_of_range& oor) {
    std::cerr << "SDLEngine::update_coins() couldn't find LABEL_COINS" << std::endl;
  }
  try {
    Sprite* sprt = sprites.at(SPRITE_COINS);
    sprt->set_animation_mode(Animation::Mode::oneway);
    sprt->set_animation_reps(2);
  } catch (const std::out_of_range& oor) {
    std::cerr << "SDLEngine::update_coins() couldn't find SPRITE_COINS" << std::endl;
  }
  try {
    Mix_PlayChannel(CHANNEL_COINS,sounds.at(SOUND_COIN_EARNED),0);
  } catch (const std::out_of_range& oor) {
    std::cerr << "Error: missing SOUND_COIN_EARNED" << std::endl;
  }
}

void SDLEngine::cleanup_data(void) {
  keybuffer.assign("");
  player.set_coins(0);
  combo = 0;
  //gamedata.clear();
}

void SDLEngine::free_sound_spell(void) {
  if (sounds.count(SOUND_SPELL) > 0) {
    Mix_FreeChunk(sounds.at(SOUND_SPELL));
    sounds.erase(SOUND_SPELL);
  }
}

void SDLEngine::free_memory(void) {
  titlescroll.clear_rows();
  storyscroll.clear_rows();
  free_labels();
  free_sprites();
  free_sketches();
  if (gamedraw) {
    delete gamedraw;
    gamedraw = nullptr;
  }
  free_sounds();
}

void SDLEngine::free_sketches(void) {
  std::vector<Sketch*>::iterator iter = sketches.begin();
  while (iter != sketches.end()) {
    delete *iter;
    iter++;
  }
  sketches.clear();
}

void SDLEngine::free_labels(void) {
  std::map<char,Text*>::iterator iterl = map_labels.begin();
  while (iterl != map_labels.end()) {
    delete iterl->second;
    iterl++;
  }
  map_labels.clear();

  std::vector<Text*>::iterator itertl = labels.begin();
  while (itertl != labels.end()) {
    delete *itertl;
    itertl++;
  }
  labels.clear();
}

void SDLEngine::free_sprites(void) {
  std::map<char,Sprite*>::iterator iters = sprites.begin();
  while (iters != sprites.end()) {
    delete iters->second;
    iters++;
  }
  sprites.clear();
  std::vector<Sprite*>::iterator itert = temp_sprites.begin();
  while (itert != temp_sprites.end()) {
    delete *itert;
    itert++;
  }
  temp_sprites.clear();
}

void SDLEngine::free_textures(void) {
  std::map<int,SDL_Texture*>::iterator it = textures.begin();
  while (it != textures.end()) {
    SDL_DestroyTexture(it->second);
    it++;
  }
  textures.clear();
}

void SDLEngine::free_sounds(void) {
  std::map<char,Mix_Chunk*>::iterator iter = sounds.begin();
  while (iter != sounds.end()) {
    Mix_FreeChunk(iter->second);
    iter++;
  }
  sounds.clear();
}

void SDLEngine::free_musics(void) {
  std::map<char,Mix_Music*>::iterator iter = musics.begin();
  while (iter != musics.end()) {
    Mix_FreeMusic(iter->second);
    iter++;
  }
  sounds.clear();
}
