#
# Makefile
# copyright 2020 Massimiliano Maniscalco
#


# compilatore da usare
CPP = g++

# opzioni da passare al compilatore
CPPFLAGS = -std=c++11

# nome dell'eseguibile
EXE = storieincomp

# lista dei file header separati dagli spazi
#HDRS = 

# lista delle librerie da utilizzare separate dagli spazi
# ogni libreria dovrà avere il prefisso -l
LIBS = -lSDL2 -lSDL2_image -lSDL2_ttf -lSDL2_mixer -lSDL2_gfx -lfontconfig

# build directory
OBJDIR := build

# automated creation of object files list from all files with .cc suffix
OBJS := $(addprefix $(OBJDIR)/, $(patsubst %.cc,%.o,$(wildcard *.cc)))

# default
$(OBJDIR)/$(EXE): $(OBJS) Makefile | $(OBJDIR)
	$(CPP) $(CPPFLAGS) -o $@ $(OBJS) $(LIBS)

$(OBJDIR) :
	mkdir $(OBJDIR)

# dipendenze 
$(OBJDIR)/main.o : main.cc sdlengine.hh | $(OBJDIR)
	$(CPP) $(CPPFLAGS) -c $< -o $@

$(OBJDIR)/timing.o : timing.cc timing.hh | $(OBJDIR)
	$(CPP) $(CPPFLAGS) -c $< -o $@

$(OBJDIR)/animation.o : animation.cc animation.hh timing.hh | $(OBJDIR)
	$(CPP) $(CPPFLAGS) -c $< -o $@

$(OBJDIR)/motion.o : motion.cc motion.hh timing.hh | $(OBJDIR)
	$(CPP) $(CPPFLAGS) -c $< -o $@

$(OBJDIR)/fading.o : fading.cc fading.hh timing.hh | $(OBJDIR)
	$(CPP) $(CPPFLAGS) -c $< -o $@

$(OBJDIR)/matrix.o : matrix.cc matrix.hh | $(OBJDIR)
	$(CPP) $(CPPFLAGS) -c $< -o $@

$(OBJDIR)/transformation.o : transformation.cc transformation.hh matrix.hh | $(OBJDIR)
	$(CPP) $(CPPFLAGS) -c $< -o $@

$(OBJDIR)/sketch.o : sketch.cc sketch.hh transformation.hh matrix.hh | $(OBJDIR)
	$(CPP) $(CPPFLAGS) -c $< -o $@

$(OBJDIR)/drawing.o : drawing.cc drawing.hh | $(OBJDIR)
	$(CPP) $(CPPFLAGS) -c $< -o $@

$(OBJDIR)/sprite.o : sprite.cc sprite.hh animation.hh motion.hh fading.hh timing.hh | $(OBJDIR)
	$(CPP) $(CPPFLAGS) -c $< -o $@

$(OBJDIR)/player.o : player.cc player.hh | $(OBJDIR)
	$(CPP) $(CPPFLAGS) -c $< -o $@

$(OBJDIR)/gamedata.o : gamedata.cc gamedata.hh story.hh storyrow.hh | $(OBJDIR)
	$(CPP) $(CPPFLAGS) -c $< -o $@

$(OBJDIR)/storyrow.o : storyrow.cc storyrow.hh | $(OBJDIR)
	$(CPP) $(CPPFLAGS) -c $< -o $@

$(OBJDIR)/story.o : story.cc story.hh storyrow.hh | $(OBJDIR)
	$(CPP) $(CPPFLAGS) -c $< -o $@

$(OBJDIR)/text.o : text.cc text.hh fading.hh timing.hh motion.hh | $(OBJDIR)
	$(CPP) $(CPPFLAGS) -c $< -o $@

$(OBJDIR)/text_scroll.o : text_scroll.cc text_scroll.hh text.hh fading.hh timing.hh motion.hh | $(OBJDIR)
	$(CPP) $(CPPFLAGS) -c $< -o $@

$(OBJDIR)/sdlengine.o : sdlengine.cc sdlengine.hh timing.hh animation.hh motion.hh fading.hh matrix.hh transformation.hh sketch.hh drawing.hh sprite.hh \
gamedata.hh storyrow.hh story.hh text.hh text_scroll.hh player.hh | $(OBJDIR)
	$(CPP) $(CPPFLAGS) -c $< -o $@

# pulizia
.PHONY: clean
clean:
	rm -f core $(OBJDIR)/$(EXE) $(OBJDIR)/*.o

# installazione
install:
	mkdir -p $(DESTDIR)/usr/bin
	install -m 0755 build/$(EXE) $(DESTDIR)/usr/bin/$(EXE)
	mkdir -p $(DESTDIR)/usr/share/$(EXE)/sounds/spells
	install -m 0664 *.txt $(DESTDIR)/usr/share/$(EXE)
	install -m 0664 sounds/*.wav $(DESTDIR)/usr/share/$(EXE)/sounds
	install -m 0664 sounds/*.mp3 $(DESTDIR)/usr/share/$(EXE)/sounds
	install -m 0664 sounds/spells/*.wav $(DESTDIR)/usr/share/$(EXE)/sounds/spells
	install -m 0644 $(EXE).desktop $(DESTDIR)/usr/share/applications
	mkdir -p $(DESTDIR)/usr/share/$(EXE)/graphics
	install -m 0664 graphics/*.png $(DESTDIR)/usr/share/$(EXE)/graphics
	install -m 0644 icons/48x48/$(EXE).png $(DESTDIR)/usr/share/icons/hicolor/48x48/apps
	install -m 0644 icons/64x64/$(EXE).png $(DESTDIR)/usr/share/icons/hicolor/64x64/apps
	install -m 0644 icons/72x72/$(EXE).png $(DESTDIR)/usr/share/icons/hicolor/72x72/apps
	install -m 0644 icons/96x96/$(EXE).png $(DESTDIR)/usr/share/icons/hicolor/96x96/apps
	install -m 0644 icons/128x128/$(EXE).png $(DESTDIR)/usr/share/icons/hicolor/128x128/apps
	install -m 0644 icons/192x192/$(EXE).png $(DESTDIR)/usr/share/icons/hicolor/192x192/apps
	install -m 0644 icons/256x256/$(EXE).png $(DESTDIR)/usr/share/icons/hicolor/256x256/apps
	install -m 0644 icons/512x512/$(EXE).png $(DESTDIR)/usr/share/icons/hicolor/512x512/apps
	gtk-update-icon-cache /usr/share/icons/hicolor

# disinstallazione
uninstall:
	rm -f $(DESTDIR)/usr/bin/$(EXE)
	rm -f -R $(DESTDIR)/usr/share/$(EXE)
	rm -f $(DESTDIR)/usr/share/applications/$(EXE).desktop
	rm -f $(DESTDIR)/usr/share/icons/hicolor/48x48/apps/$(EXE).png
	rm -f $(DESTDIR)/usr/share/icons/hicolor/64x64/apps/$(EXE).png
	rm -f $(DESTDIR)/usr/share/icons/hicolor/72x72/apps/$(EXE).png
	rm -f $(DESTDIR)/usr/share/icons/hicolor/96x96/apps/$(EXE).png
	rm -f $(DESTDIR)/usr/share/icons/hicolor/128x128/apps/$(EXE).png
	rm -f $(DESTDIR)/usr/share/icons/hicolor/192x192/apps/$(EXE).png
	rm -f $(DESTDIR)/usr/share/icons/hicolor/256x256/apps/$(EXE).png
	rm -f $(DESTDIR)/usr/share/icons/hicolor/512x512/apps/$(EXE).png
