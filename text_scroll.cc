/*
 * text_scroll.cc
 * Copyright (C) 2020 Massimiliano Maniscalco
 * 
 * This file is part of DoppiAvventura
 *
 * This is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "text_scroll.hh"
#include <iostream>

TextScroll::TextScroll() : max_rows (10) {

}

TextScroll::~TextScroll() {
  clear_rows();
}

void TextScroll::add_row(Text* txt, Alignment al) {
  SDL_Rect pos = txt->get_position();
  int last_row = rows.size() > max_rows ? max_rows : rows.size();
  pos.x = al == Alignment::centre ? rect.x + ((rect.w - pos.w) / 2) : rect.x;
  pos.y = rect.y + (last_row * pos.h);
  txt->set_position(&pos);
  txt->set_speed({ 0.0, 2.0 });
  txt->set_fading_mode(Fading::Mode::in);
  txt->set_alpha(1);
  rows.push_back(txt);
  if (rows.size() >= max_rows) {
    txt->add_destination({ pos.x, pos.y - pos.h });
    fade_out(1);
    scroll();
  }
}

void TextScroll::set_text(const std::vector<Text*>& vec, Alignment al) {
  clear_rows();
  int n = 0;
  std::vector<Text*>::const_iterator iter = vec.cbegin();
  while (iter != vec.cend()) {
    SDL_Rect pos = (*iter)->get_position();
    pos.x = al == Alignment::centre ? rect.x + ((rect.w - pos.w) / 2) : rect.x;
    pos.y = rect.y + rect.h + (n * pos.h);
    (*iter)->set_position(&pos);
    (*iter)->set_alpha(1);
    (*iter)->set_fading_pace(10.0);
    (*iter)->set_speed({ 0.0, 0.25 });
    (*iter)->add_destination({ pos.x, pos.y - pos.h });
    rows.push_back(*iter);
    n++;
    iter++;
  }
  iter = rows.begin();
  if (iter != rows.end()) {
    (*iter)->set_fading_mode(Fading::Mode::in);
  }
}

const Text* TextScroll::get_last_row(void) const {
  if (rows.size() > 0) {
    return rows.back();
  }
  return nullptr;
}

void TextScroll::fade_out(std::vector<Text*>* vec, int n) {
  int i = 0;
  std::vector<Text*>::iterator iter = vec->begin();
  while (iter != vec->end() && i < n) {
    if ((*iter)->get_fading_mode() == Fading::Mode::none) {
      (*iter)->set_fading_mode(Fading::Mode::out);
      i++;
    }
    iter++;
  }
}

void TextScroll::scroll(std::vector<Text*>* vec) {
  std::vector<Text*>::iterator iter = vec->begin();
  while (iter != vec->end()) {
    SDL_Rect pos = (*iter)->get_position();
    if (!(*iter)->is_moving()) {
      (*iter)->add_destination({ pos.x, pos.y - pos.h });
    }
    iter++;
  }
}

bool TextScroll::is_ready(void) {
  std::vector<Text*>::iterator ittl = rows.begin();
  while (ittl != rows.end()) {
    if (*ittl) {
      if ((*ittl)->is_moving() || (*ittl)->get_fading_mode() != Fading::Mode::none) {
	return false;
      }
    }
    ittl++;
  }
  return true;
}

void TextScroll::update(void) {
  std::vector<Text*>::iterator itl = rows.begin();
  while (itl != rows.end()) {
    if (*itl != nullptr) {
      (*itl)->move();
      (*itl)->fade();
      if ((*itl)->get_alpha() == 0) {
	delete *itl;
	*itl = nullptr;
	itl = rows.erase(itl);
      } else {
	itl++;
      }
    } else {
      itl++;
    }
  }
}

void TextScroll::update_autoscroll(void) {
  std::vector<Text*>::iterator itl = rows.begin();
  while (itl != rows.end()) {
    if (*itl != nullptr) {
      (*itl)->move();
      (*itl)->fade();
      SDL_Rect pos = (*itl)->get_position();
      if (pos.y < rect.y) {
	pos.y = rect.y + rect.h + (rows.size() * pos.h);
	(*itl)->set_position(&pos);
	(*itl)->set_alpha(1);
      } else if (pos.y < rect.y + pos.h) {
	if ((*itl)->get_fading_mode() == Fading::Mode::none) {
	  (*itl)->set_fading_mode(Fading::Mode::out);
	}
      } else if (pos.y < rect.y + rect.h + pos.h) {
	if ((*itl)->get_alpha() <= 1 && (*itl)->get_fading_mode() == Fading::Mode::none) {
	  (*itl)->set_fading_mode(Fading::Mode::in);
	}
      }
      if (!(*itl)->is_moving()) {
	(*itl)->add_destination({ pos.x, pos.y - pos.h });
      }
    }
    itl++;
  }
}

void TextScroll::draw(void) {
  std::vector<Text*>::iterator ittl = rows.begin();
  while (ittl != rows.end()) {
    if (*ittl) {
      (*ittl)->draw();
    }
    ittl++;
  }
}

void TextScroll::clear_rows(void) {
  std::vector<Text*>::iterator iterl = rows.begin();
  while (iterl != rows.end()) {
    delete *iterl;
    iterl++;
  }
  rows.clear();
}
