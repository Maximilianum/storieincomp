/*
 * drawing.cc
 * Copyright (C) 2020 Massimiliano Maniscalco
 * 
 * This is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "drawing.hh"
#include <iostream>

Drawing::Drawing(void) {
  renderer = nullptr;
  backcol = { 255, 255, 255, 255 };
}

void Drawing::set_pattern(const std::vector<std::pair<int, SDL_Colour>>& vec) {
  pattern.clear();
  pattern.insert(pattern.begin(), vec.begin(), vec.end());
}

void Drawing::set_lines(const std::vector<std::pair<SDL_Rect, SDL_Colour>>& vec) {
  lines.clear();
  lines.insert(lines.begin(), vec.begin(), vec.end());
}

void Drawing::create_pattern(void) {
  lines.clear();
  int n = height / nrpt;
  int y = 0;
  for (int i = 0; i < nrpt; i++) {
    std::vector<std::pair<int, SDL_Colour>>::const_iterator it = pattern.cbegin();
    while (it != pattern.cend()) {
      lines.push_back({ { 0, y + it->first, width, y + it->first }, { it->second.r, it->second.g, it->second.b, it->second.a } });
      it++;
    }
    y += height / nrpt;
  }
}

void Drawing::animate_pattern(Mode mode) {
  if (timing.check_time()) {
    std::vector<std::pair<SDL_Rect, SDL_Colour>>::iterator iter = lines.begin();
    while (iter != lines.end()) {
      if (mode == Mode::down) {
	iter->first.y++;
	if (iter->first.y > height) {
	  iter->first.y -= height;
	}
      } else if (mode == Mode::up) {
	iter->first.y--;
	if (iter->first.y < 0) {
	  iter->first.y += height;
	}
      }
      iter->first.h = iter->first.y;
      iter++;
    }
  }
}

void Drawing::draw(void) {
  if (renderer) {
    SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_BLEND);
    SDL_SetRenderDrawColor(renderer,backcol.r,backcol.g,backcol.b,backcol.a);
    SDL_RenderClear(renderer);

    std::vector<std::pair<SDL_Rect, SDL_Colour>>::const_iterator iter = lines.cbegin();
    while (iter != lines.cend()) {
      SDL_SetRenderDrawColor(renderer, iter->second.r, iter->second.g, iter->second.b, iter->second.a);
      SDL_RenderDrawLine(renderer, iter->first.x, iter->first.y, iter->first.w, iter->first.h);
      iter++;
    }
  }
}

