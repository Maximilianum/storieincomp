/*
 * animation.h
 * Copyright (C) 2020 Massimiliano Maniscalco
 *
 * This file is part of DoppiAvventura
 * 
 * This is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _ANIMATION_H_
#define _ANIMATION_H_

#include "timing.hh"

class Animation {
 public:
  enum class Mode { none, oneway, twoway, loop1way, loop2way };
  enum class Way { forward, backward };
  Animation(void) : current_frame (0), way (1), mode (Mode::none), repetition (0) {}
  Animation(int tf) : total_frames (tf), current_frame (0), way (1), mode (Mode::none) {}
  inline void set_current_frame(int val) { current_frame = val; }
  inline int get_current_frame(void) const { return current_frame; }
  inline void set_total_frames(int val) { total_frames = val; }
  inline int get_total_frames(void) const { return total_frames; }
  inline void set_mode(Mode md) { mode = md; }
  inline Mode get_mode(void) const { return mode; }
  inline void set_way(Way w) { way = w == Way::forward ? 1 : -1; }
  inline Way get_way(void) const { return way == 1 ? Way::forward: Way::backward; }
  inline void set_repetition(unsigned short val) { repetition = val; }
  bool animate(void);
  
 private:
  Timing timing;
  int current_frame;
  int way;
  int total_frames;
  Mode mode;
  unsigned short repetition;
  
};

#endif // _ANIMATION_H_
