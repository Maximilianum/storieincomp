/*
 * transformation.h
 * Copyright (C) 2020 Massimiliano Maniscalco
 * 
 * This file is part of DoppiAvventura
 *
 * This is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _TRANSFORMATION_H_
#define _TRANSFORMATION_H_

#include <SDL2/SDL.h>
#include <vector>
#include "matrix.hh"

class Transformation {
 public:

  class TransformationError {  
  public:
    TransformationError(const std::string& str) : message (str) {}
    inline std::string get_message(void) const { return message; }
  private:
    const std::string message;
  };
  
  typedef struct {
    Sint16* vx;
    Sint16* vy;
    int size;
    SDL_Colour col;
  } Vertexes;

  static Vertexes transform(const Vertexes& vert, Matrix& matrix);
  Transformation();
  ~Transformation();
  void set_current_matrix(int index);
  inline void add_matrix(void) { matrixes.push_back(Matrix::get_identity()); }
  inline int count(void) const { return matrixes.size(); }
  void mult_by_translation(double cx, double cy, int index = 0);
  void mult_by_rotate(double a, int index = 0);
  void mult_by_scale(double cx, double cy, int index = 0);
  void mult_by_shear(double cx, double cy, int index = 0);
  void clear(void);
  bool transform(const std::vector<Vertexes>* vec);
  inline const std::vector<Vertexes>* get_vertexes(void) const { return &transformed; }
  inline bool is_identity(void) const { return transformed.size() == 0; }
 protected:
  void clear_vertexes(void);
 private:
  std::vector<Vertexes> transformed;
  std::vector<Matrix> matrixes;
  std::vector<Matrix>::iterator curr_matrix;
};

#endif // _TRANSFORMATION_H_
