/*
 * gamedata.cc
 * Copyright (C) 2020 Massimiliano Maniscalco
 * 
 * This is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "gamedata.hh"
#include <fstream>

std::istream& operator>>(std::istream& is, std::vector<std::string>& str_vec) {
  while (is.good()) {
    std::string str;
    is >> str;
    if (is.good()) {
      str_vec.push_back(str);
    }
  }
  return is;
}

GameData* GameData::load_stories(const std::string& path) {
  GameData* gd { new GameData() };
  gd->set_path(path);
  std::ifstream infile;
  std::string spath { path + std::string("stories.txt") };
  infile.open(spath.c_str(), std::ifstream::in);
  if (infile) {
    std::vector<std::string> vec;
    infile >> vec;
    if (infile.fail() && !infile.eof()) {
      std::cerr << "load error file " << spath << std::endl;
    } else {
      gd->set_stories(vec);
    }
  } else {
    std::cerr << "unable to open file " << spath << std::endl;
  }
  infile.close();
  gd->load_titles();
  return gd;
}

GameData::GameData() : curr_story (0) {

}

void GameData::set_stories(const std::vector<std::string>& vec) {
  stories.clear();
  stories.insert(stories.begin(), vec.begin(), vec.end());
}

void GameData::load_titles(void) {
  titles.clear();
  std::vector<std::string>::const_iterator iter = stories.cbegin();
  while (iter != stories.cend()) {
    std::ifstream infile;
    std::string spath { path + *iter + std::string(".txt") };
    infile.open(spath.c_str(), std::ifstream::in);
    if (infile) {
      StoryRow sr;
      infile >> sr;
      if (infile.fail() && !infile.eof()) {
	std::cerr << "load error file " << spath << std::endl;
      } else {
	titles.push_back(sr.get_sentence());
      }
    } else {
      std::cerr << "unable to open file " << spath << std::endl;
    }
    infile.close();
    iter++;
  }
}

void GameData::load_story(void) {
  story.clear();
  std::ifstream infile;
  try {
    std::string spath { path + stories.at(curr_story) + std::string(".txt") };
    infile.open(spath.c_str(), std::ifstream::in);
    if (infile) {
      infile >> story;
      if (infile.fail() && !infile.eof()) {
	std::cerr << "load error file " << spath << std::endl;
      }
    } else {
      std::cerr << "unable to open file " << spath << std::endl;
    }
  } catch (const std::out_of_range& oor) {
    std::cerr << "GameData::load_story() out of range error" << std::endl;
  }
  infile.close();
}

void GameData::clear(void) {
  curr_story = 0;
}

