/*
 * timing.cc
 * Copyright (C) 2020 Massimiliano Maniscalco
 *
 * This file is part of DoppiAvventura
 * 
 * This is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "timing.hh"

int Timing::fps = 60;

bool Timing::check_time(void) {
  if (old_time + (1000 / fps) > SDL_GetTicks()) {
    return false;
  }
  old_time = SDL_GetTicks();
  return true;
}
