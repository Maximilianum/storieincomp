
/*
 * sketch.cc
 * Copyright (C) 2020 Massimiliano Maniscalco
 * 
 * This is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "sketch.hh"
#include <cmath>
#include <iostream>

Transformation::Vertexes Sketch::create_star(int x, int y, int size, const SDL_Colour& col) {
  const double da = (2.0 * PI) / 5.0;
  double rb = size / 2.0; // ray big
  double rl = rb / 2.5; // ray little
  double ag = - PI / 2.0; // big circle's starting angle
  double al = - PI / 3.3; // little circle's starting angle 
  Sint16* vx = (Sint16*) calloc(10, sizeof(Sint16));
  Sint16* vy = (Sint16*) calloc(10, sizeof(Sint16));
  for (int i = 0; i < 10; i += 2) {
    vx[i] = x + rb + std::round(rb * std::cos(ag));
    vy[i] = y + rb + std::round(rb * std::sin(ag));
    ag += da;
    vx[i + 1] = x + rb + std::round(rl * std::cos(al));
    vy[i + 1] = y + rb + std::round(rl * std::sin(al));
    al += da;
  }
  return { vx, vy, 10, col };
}

std::vector<Transformation::Vertexes> Sketch::create_flower(int x, int y, int size, const SDL_Colour& col1, const SDL_Colour& col2) {
  std::vector<Transformation::Vertexes> vec;
  double a = size / 6;
  double b = size / 10;
  double c = std::sqrt(std::pow(a, 2.0) - std::pow(b, 2.0));
  double f1a = ((2.0 * a) - (2.0 * c)) / 2.0;
  double d = c + f1a;
  Matrix matrix;
  vec.push_back(create_circle(size / 2, size / 2, size / 6, 24, col1));
  Transformation::Vertexes petal { create_ellipse(d, b, a, b, 24, col2) };
  matrix = Matrix::get_translation(-d, -b) * matrix;
  matrix = Matrix::get_rotate(PI / -2.0) * matrix;
  matrix = Matrix::get_translation(size / 2, d) * matrix;
  vec.push_back(Transformation::transform(petal, matrix));
  matrix = Matrix::get_identity();
  matrix = Matrix::get_translation(-d, -b) * matrix;
  matrix = Matrix::get_rotate(PI / -4.0) * matrix;
  matrix = Matrix::get_translation(((size * 3) / 5) + (b * 1.5), d * 1.5) * matrix;
  vec.push_back(Transformation::transform(petal, matrix));
  matrix = Matrix::get_identity();
  matrix = Matrix::get_translation(size - (2 * d), (size / 2) - b) * matrix;
  vec.push_back(Transformation::transform(petal, matrix));
  matrix = Matrix::get_identity();
  matrix = Matrix::get_translation(-d, -b) * matrix;
  matrix = Matrix::get_rotate(PI / 4.0) * matrix;
  matrix = Matrix::get_translation(((size * 3) / 5) + (b * 1.5), ((size * 3) / 5) + (b * 1.5)) * matrix;
  vec.push_back(Transformation::transform(petal, matrix));
  matrix = Matrix::get_identity();
  matrix = Matrix::get_translation(-d, -b) * matrix;
  matrix = Matrix::get_rotate(PI / 2.0) * matrix;
  matrix = Matrix::get_translation(size / 2, size - d) * matrix;
  vec.push_back(Transformation::transform(petal, matrix));
  matrix = Matrix::get_identity();
  matrix = Matrix::get_translation(-d, -b) * matrix;
  matrix = Matrix::get_rotate(PI / -4.0) * matrix;
  matrix = Matrix::get_translation(d * 1.5, ((size * 3) / 5) + (b * 1.5)) * matrix;
  vec.push_back(Transformation::transform(petal, matrix));
  matrix = Matrix::get_identity();
  matrix = Matrix::get_translation(0, (size / 2) - b) * matrix;
  vec.push_back(Transformation::transform(petal, matrix));
  matrix = Matrix::get_identity();
  matrix = Matrix::get_translation(-d, -b) * matrix;
  matrix = Matrix::get_rotate(PI / 4.0) * matrix;
  matrix = Matrix::get_translation(d * 1.5, d * 1.5) * matrix;
  vec.push_back(Transformation::transform(petal, matrix));
  free(petal.vx);
  free(petal.vy);
  return vec;
}

Sketch::Sketch(SDL_Renderer* rndr, const SDL_Rect& src) : renderer (rndr), source (src), texture (nullptr) {
  anime.set_total_frames(transformation.count());
}

Sketch::~Sketch() {
  if (texture) {
    SDL_DestroyTexture(texture);
    texture = nullptr;
  }
  clear_drawings();
}

void Sketch::set_drawings(const std::vector<Transformation::Vertexes>& vec) {
  clear_drawings();
  drawings.insert(drawings.begin(), vec.begin(), vec.end());
}

void Sketch::add_transformation(void) {
  transformation.add_matrix();
  anime.set_total_frames(transformation.count());
}

void Sketch::clear_transformations(void) {
  transformation.clear();
  anime.set_total_frames(transformation.count());
}

bool Sketch::transform(void) {
  bool r = transformation.transform(&drawings);
  if (r) {
    update_texture();
  }
  return r;
}

void Sketch::update_texture(void) {
  if (texture == nullptr) {
    texture = create_texture(*this);
  }
  SDL_SetRenderTarget(renderer, texture);
  SDL_SetTextureBlendMode(texture, SDL_BLENDMODE_BLEND);
  SDL_SetRenderDrawColor(renderer,255,255,255,0);
  SDL_RenderClear(renderer);
  const std::vector<Transformation::Vertexes>* vec = nullptr;
  if (!transformation.is_identity()) {
    vec = transformation.get_vertexes();
  } else {
    vec = &drawings;
  }
  std::vector<Transformation::Vertexes>::const_iterator iter = vec->cbegin();
  while (iter != vec->cend()) {
    filledPolygonRGBA(renderer, iter->vx, iter->vy, iter->size, iter->col.r, iter->col.g, iter->col.b, iter->col.a);
    iter++;
  }
  SDL_SetRenderTarget(renderer, nullptr);
}

void Sketch::draw(void) {
  if (!texture) {
    update_texture();
  }
  SDL_SetTextureAlphaMod(texture, fading.get_alpha());
  SDL_RenderCopy(renderer, texture, &source, &position);
  //SDL_SetRenderDrawColor(renderer,0,0,255,255);
  //SDL_RenderDrawRect(renderer, &position);
}

bool Sketch::animate(void) {
  bool r = anime.animate();
  if (r) {
    transformation.set_current_matrix(anime.get_current_frame());
    transform();
  }
  return r;
}

void Sketch::clear_drawings(void) {
  std::vector<Transformation::Vertexes>::iterator iter = drawings.begin();
  while (iter != drawings.end()) {
    if (iter->vx) {
      free(iter->vx);
      iter->vx = nullptr;
    }
    if (iter->vy) {
      free(iter->vy);
      iter->vy = nullptr;
    }
    iter->size = 0;
    iter++;
  }
  drawings.clear();
}

SDL_Texture* Sketch::create_texture(const Sketch& sktc) {
  SDL_Texture* txtr = nullptr;
  txtr = SDL_CreateTexture(sktc.renderer, SDL_PIXELFORMAT_RGBA32, SDL_TEXTUREACCESS_TARGET, sktc.source.w, sktc.source.h);
  if (txtr) {
    SDL_SetRenderTarget(sktc.renderer, txtr);
    SDL_SetTextureBlendMode(txtr, SDL_BLENDMODE_BLEND);
    SDL_SetRenderDrawColor(sktc.renderer,255,255,255,0);
    SDL_RenderClear(sktc.renderer);
    SDL_SetRenderTarget(sktc.renderer, nullptr);
  } else {
    throw SketchError { std::string { "Sketch error: failed to create SDL_Texture" } };
  }
  return txtr;
}

// x, y offset coordinates, radius, number of points, colour
Transformation::Vertexes Sketch::create_circle(int x, int y, int r, int np, const SDL_Colour& col) {
  double da = (2 * PI) / np;
  double a = - PI / 2.0;
  Sint16* vx = (Sint16*) calloc(np, sizeof(Sint16));
  Sint16* vy = (Sint16*) calloc(np, sizeof(Sint16));
  for (int i = 0; i < np; i++) {
    vx[i] = x + std::round(r * std::cos(a));
    vy[i] = y + std::round(r * std::sin(a));
    a += da;
  }
  return { vx, vy, np, col };
}

// x, y offset coordinates, a, b, number of points, colour
Transformation::Vertexes Sketch::create_ellipse(int x, int y, double a, double b, int np, const SDL_Colour& col) {
  double da = (2 * PI) / np;
  double an = - PI / 2.0;
  Sint16* vx = (Sint16*) calloc(np, sizeof(Sint16));
  Sint16* vy = (Sint16*) calloc(np, sizeof(Sint16));
  for (int i = 0; i < np; i++) {
    vx[i] = x + std::round(a * std::cos(an));
    vy[i] = y + std::round(b * std::sin(an));
    an += da;
  }
  return { vx, vy, np, col };
}
