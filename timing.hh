/*
 * timing.h
 * Copyright (C) 2020 Massimiliano Maniscalco
 *
 * This file is part of DoppiAvventura
 * 
 * This is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _TIMING_H_
#define _TIMING_H_

#include <SDL2/SDL.h>

class Timing {
 public:
  static inline void set_frames_per_second(int val) { fps = val; }
  static int get_frames_per_second(void) { return fps; }
 Timing() : old_time (0) {}
  bool check_time(void);
 private:
  static int fps; // frames per second
  unsigned int old_time;
};

#endif // _TIMING_H_
