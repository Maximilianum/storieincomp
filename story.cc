/*
 * story.cc
 * Copyright (C) 2020 Massimiliano Maniscalco
 * 
 * This is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "story.hh"
#include <fstream>

std::istream& operator>>(std::istream& is, std::vector<StoryRow>& vec) {
  while (is.good()) {
    StoryRow sr;
    is >> sr;
    if (is.good()) {
      vec.push_back(sr);
    }
  }
  return is;
}

std::istream& operator>>(std::istream& is, Story& stry) {
  StoryRow sr;
  is >> sr;
  if (is.good()) {
    stry.title = sr.get_sentence();
    if (is.good()) {
      is >> stry.rows;
    }
  }
  return is;
}

Story::Story() {
  clear();
}

std::string Story::get_current_sentence(void) {
  try {
    return rows.at(index).get_spaced_sentence();
  } catch (const std::out_of_range& oor) {
    return std::string();
  }
}

std::string Story::get_first_half_sentence(void) {
  try {
    return rows.at(index).get_first_half();
  } catch (const std::out_of_range& oor) {
    return std::string();
  }
}

std::string Story::get_current_spell_path(void) {
  try {
    return rows.at(index).get_word() + std::string(".wav");
  } catch (const std::out_of_range& oor) {
    return std::string();
  }
}

bool Story::set_current_index(int val) {
  if (val < rows.size()) {
    index = val;
    return true;
  }
  return false;
}

bool Story::next_sentence(void) {
  if (index < rows.size() - 1) {
    index++;
    return true;
  }
  return false;
}

std::string Story::get_current_word(void) const {
  try {
    return rows.at(index).get_word();
  } catch (const std::out_of_range& oor) {
    return std::string();
  }
}

bool Story::check_current_word(const std::string& str) {
  try {
    return rows.at(index).check(str);
  } catch (const std::out_of_range& oor) {
    return false;
  }
}

void Story::load(const std::string& path) {
  std::ifstream infile;
  infile.open(path.c_str(), std::ifstream::in);
  if (infile) {
    infile >> rows;
    if (infile.fail() && !infile.eof()) {
      std::cerr << "load error file " << path << std::endl;
    }
  } else {
    std::cerr << "unable to open file " << path << std::endl;
  }
  infile.close();
  index = 0;
}

void Story::clear(void) {
  index = 0;
  rows.clear();
}

void Story::log(void) {
  std::cout << rows.size() << " sentences." << std::endl;
  std::vector<StoryRow>::iterator iter = rows.begin();
  while (iter != rows.end()) {
    std::cout << iter->get_sentence() << " -> " << iter->get_word() << std::endl;
    iter++;
  }
}
