/*
 * storyrow.cc
 * Copyright (C) 2020 Massimiliano Maniscalco
 * 
 * This is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "storyrow.hh"

std::istream& operator>>(std::istream& is, StoryRow& sr) {
  enum class Brackets { none, start, end };
  Brackets bk = Brackets::none;
  sr.sentence.assign(std::string());
  sr.word.assign(std::string());
  while (is.good() && bk != Brackets::end) {
    std::string str;
    is >> str;
    if (is.good()) {
      switch (bk) {
      case Brackets::none:
	if (str.front() == '"') {
	  bk = Brackets::start;
	  if (str.size() > 1) {
	    str.assign(str.substr(1));
	  }
	}
	break;
      case Brackets::start:
	if (str.back() == '"') {
	  bk = Brackets::end;
	  if (str.size() > 1) {
	    str.assign(str.substr(0, str.size() - 1));
	  }
	}
	break;
      }
      if (str.compare(std::string("\"")) != 0) {
	if (sr.sentence.size() > 0) {
	  sr.sentence.append(std::string(" "));
	}
	sr.sentence.append(str);
      }
    }
  }
  if (is.good()) {
    is >> sr.word;
  }
  return is;
}

StoryRow::StoryRow() {
  sound = nullptr;
}

StoryRow::~StoryRow() {
  if (sound) {
    Mix_FreeChunk(sound);
  }
}

std::string StoryRow::get_spaced_sentence(void) const {
  std::size_t pos = sentence.find('_');
  if (pos != std::string::npos) {
    std::string first { sentence.substr(0, pos) };
    std::string second { sentence.substr(pos + 1) };
    return first + std::string(word.size() * 2.0, '_') + second;
  }
  return sentence;
}

std::string StoryRow::get_first_half(void) const {
  std::size_t pos = sentence.find('_');
  if (pos != std::string::npos) {
    return sentence.substr(0, pos);
  }
  return sentence;
}
