/*
 * story.h
 * Copyright (C) 2020 Massimiliano Maniscalco
 * 
 * This is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _STORY_H_
#define _STORY_H_

#include <iostream>
#include <vector>
#include "storyrow.hh"

class Story {
  friend std::istream& operator>>(std::istream& is, std::vector<StoryRow>& vec);
  friend std::istream& operator>>(std::istream& is, Story& stry);
 public:
  Story();
  inline void set_title(const std::string& str ) { title = str; }
  std::string get_title(void) const { return title; }
  std::string get_current_sentence(void);
  std::string get_first_half_sentence(void);
  std::string get_current_spell_path(void);
  inline int get_current_index(void) const { return index; }
  bool set_current_index(int val);
  bool next_sentence(void);
  std::string get_current_word(void) const;
  bool check_current_word(const std::string& str);
  inline int size(void) const { return rows.size(); }
  void load(const std::string& path);
  void clear(void);
  void log(void);
 private:
  std::string title;
  std::vector<StoryRow> rows;
  int index;
};

#endif // _STORY_H_

