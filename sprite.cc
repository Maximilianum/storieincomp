/*
 * sprite.cc
 * Copyright (C) 2020 Massimiliano Maniscalco
 * 
 * This file is part of DoppiAvventura
 *
 * This is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "sprite.hh"

Sprite::Sprite(SDL_Renderer* rndr, SDL_Texture* txtr) : renderer (rndr), texture (txtr), visible (true), sensitive (false) {
  if (!renderer) {
    throw SpriteError { std::string { "Sprite error: SDL_Renderer is null" } };
  }
  if (!texture) {
    throw SpriteError { std::string { "Sprite error: SDL_Texture is null" } };
  }
}

void Sprite::draw(void) {
  if (visible) {
    if (!texture) {
      throw SpriteError { std::string { "Sprite error: texture is null" } };
    }
    int frm = anime.get_current_frame();
    SDL_Rect src { source.x, source.y + (source.h * frm), source.w, source.h };
    SDL_SetTextureAlphaMod(texture, fading.get_alpha());
    SDL_RenderCopy(renderer, texture, &src, &position);
  }
}

bool Sprite::check_position(const SDL_Point* sp) {
  if (sensitive) {
    return SDL_PointInRect(sp, &position);
  }
  return false;
}

