/*
 * fading.h
 * Copyright (C) 2020 Massimiliano Maniscalco
 * 
 * This file is part of DoppiAvventura
 *
 * This is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _FADING_H_
#define _FADING_H_

#include "timing.hh"

class Fading {
 public:
  enum class Mode { none, in, out, in_out, in_loop, out_loop, in_out_loop };
  enum class Way { in, out };
 Fading() : alpha (255), mode (Mode::none), pace (5.0), acceleration (0.0) {}
  inline void set_alpha(unsigned char val) { alpha = val; }
  inline unsigned char get_alpha(void) const { return std::round(alpha); }
  void set_mode(Mode md);
  inline Mode get_mode(void) const { return mode; }
  void set_pace(double val);
  inline double get_pace(void) const { return pace; }
  inline void set_acceleration(double val) { acceleration = val; }
  bool fade(void);
  
 private:
  Timing timing;
  double alpha;
  Mode mode;
  double pace;
  double acceleration; // how much pace should change in time
};

#endif // _FADING_H_
