/*
 * text.h
 * Copyright (C) 2019 - 2020 Massimiliano Maniscalco
 * 
 * This is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _TEXT_H_
#define _TEXT_H_

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <string>
#include "fading.hh"
#include "motion.hh"

class Text {
 public:

  class TextError {  
  public:
    TextError(const std::string& str) : message (str) {}
    inline std::string get_message(void) const { return message; }
  private:
    const std::string message;
  };
  
  Text(SDL_Renderer* rndr, const std::string& str, TTF_Font* fnt, SDL_Colour clr);
  ~Text();
  inline void set_position(const SDL_Rect* rct) { position = *rct; };
  inline SDL_Rect get_position(void) const { return position; }
  inline std::string get_string(void) const { return text; }
  inline SDL_Colour get_colour(void) const { return colour; }
  bool get_size(int* w, int* h);
  void draw() const;
  void set_random_alpha(void);
  void update_texture(int matched, TTF_Font* fnt, const SDL_Colour& col);
  void update_texture(const std::string& str, const SDL_Colour& col);
  void rebuild_texture(const std::string& str, const SDL_Colour& col);
  inline void set_alpha(unsigned char val) { fading.set_alpha(val); }
  inline char get_alpha(void) const { return fading.get_alpha(); }
  inline void set_fading_mode(Fading::Mode md) { fading.set_mode(md); }
  inline Fading::Mode get_fading_mode(void) const { return fading.get_mode(); }
  inline void set_fading_pace(double val) { fading.set_pace(val); }
  inline double get_fading_pace(void) const { return fading.get_pace(); }
  inline void set_fading_acceleration(double val) { fading.set_acceleration(val); }
  inline bool fade(void) { return fading.fade(); }
  inline void add_destination(const SDL_Point& pnt) { motion.add_destination(pnt); }
  inline void clear_destinations(void) { motion.clear_destinations(); }
  inline bool is_moving(void) const { return motion.is_moving(); }
  inline void set_speed(const Motion::Speed& spd) { motion.set_speed(spd); }
  inline Motion::Speed get_speed(void) const { return motion.get_speed(); }
  inline bool move(void) { return motion.move(&position); }

 protected:
  std::string text;
  SDL_Renderer* renderer;
  SDL_Texture* texture;
  SDL_Colour colour;
  SDL_Rect source;
  SDL_Rect position;
  TTF_Font* font;
  Fading fading;
  Motion motion;

 private:
  void create_texture(SDL_Texture** txtr, const std::string& str, const SDL_Colour& col);

};

#endif // _TEXT_H_
