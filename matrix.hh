/*
 * matrix.h
 * Copyright (C) 2020 Massimiliano Maniscalco
 * 
 * This is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _MATRIX_H_
#define _MATRIX_H_

#include <vector>
#include <string>
#include <cmath>

class Matrix {
 public:

  class MatrixError {  
  public:
    MatrixError(const std::string& str) : message (str) {}
    inline std::string get_message(void) const { return message; }
  private:
    const std::string message;
  };
  static inline Matrix get_identity(void) { return Matrix { 1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0 }; }
  static inline Matrix get_translation(double cx, double cy) { return Matrix { 1.0, 0.0, cx, 0.0, 1.0, cy, 0.0, 0.0, 1.0 }; }
  static inline Matrix get_scale(double cx, double cy) { return Matrix { cx, 0.0, 0.0, 0.0, cy, 0.0, 0.0, 0.0, 1.0 }; }
  static inline Matrix get_rotate(double a) { return Matrix { std::cos(a), - std::sin(a), 0.0, std::sin(a), std::cos(a), 0.0, 0.0, 0.0, 1.0 }; }
  static inline Matrix get_shear(double cx, double cy) { return Matrix { 1.0, cx, 0.0, cy, 1.0, 0.0, 0.0, 0.0, 1.0 }; }
  Matrix();
  Matrix(std::initializer_list<double> values);
  Matrix& operator=(const Matrix& mtx);
  Matrix& operator*(const Matrix& mtx);
  bool operator==(const Matrix& mtx);
  std::vector<double> operator*(const std::vector<double>& values);
  void log(void) const;
 private:
  double matrix[3][3];
};

#endif // _MATRIX_H_
