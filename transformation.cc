/*
 * transformation.cc
 * Copyright (C) 2020 Massimiliano Maniscalco
 * 
 * This file is part of DoppiAvventura
 *
 * This is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "transformation.hh"
#include <stdexcept>

Transformation::Vertexes Transformation::transform(const Vertexes& vert, Matrix& matrix) {
  Sint16* vx = (Sint16*) calloc(vert.size, sizeof(Sint16));
  Sint16* vy = (Sint16*) calloc(vert.size, sizeof(Sint16));
  for (int i = 0; i < vert.size; i++) {
    std::vector<double> vrtx;      // vector used to compute matrix transformation
    vrtx.push_back(vert.vx[i]);   // copy values to vector
    vrtx.push_back(vert.vy[i]);
    vrtx.push_back(1.0);
    vrtx = matrix * vrtx;          // apply transformation
    vx[i] = vrtx[0];                // copy back transformed values
    vy[i] = vrtx[1];
  }
  return { vx, vy, vert.size, vert.col };
}

Transformation::Transformation() {
  matrixes.push_back(Matrix::get_identity());
  curr_matrix = matrixes.begin();
}

Transformation::~Transformation() {
  clear_vertexes();
}

void Transformation::set_current_matrix(int index) {
  if (index < matrixes.size()) {
    curr_matrix = matrixes.begin() + index;
  } else {
    throw TransformationError { std::string { "TransformationError set_current_matrix() index out of range" } };
  }
}

void Transformation::mult_by_translation(double cx, double cy, int index) {
  Matrix addmatrix = Matrix::get_translation(cx, cy);
  if (index < matrixes.size()) {
    std::vector<Matrix>::iterator iter = matrixes.begin() + index;
    *iter = addmatrix * (*iter);
  } else {
    throw TransformationError { std::string { "TransformationError mult_by_translation() index out of range" } };
  }
}

void Transformation::mult_by_rotate(double a, int index) {
  Matrix addmatrix = Matrix::get_rotate(a);
  if (index < matrixes.size()) {
    std::vector<Matrix>::iterator iter = matrixes.begin() + index;
    *iter = addmatrix * (*iter);
  } else {
    throw TransformationError { std::string { "TransformationError mult_by_rotate() index out of range" } };
  }
}

void Transformation::mult_by_scale(double cx, double cy, int index) {
  Matrix addmatrix = Matrix::get_scale(cx, cy);
  if (index < matrixes.size()) {
    std::vector<Matrix>::iterator iter = matrixes.begin() + index;
    *iter = addmatrix * (*iter);
  } else {
    throw TransformationError { std::string { "TransformationError mult_by_scale() index out of range" } };
  }
}

void Transformation::mult_by_shear(double cx, double cy, int index) {
  Matrix addmatrix = Matrix::get_shear(cx, cy);
  if (index < matrixes.size()) {
    std::vector<Matrix>::iterator iter = matrixes.begin() + index;
    *iter = addmatrix * (*iter);
  } else {
    throw TransformationError { std::string { "TransformationError mult_by_shear() index out of range" } };
  }
}

void Transformation::clear(void) {
  clear_vertexes();
  matrixes.clear();
  matrixes.push_back(Matrix::get_identity());
  curr_matrix = matrixes.begin();
}

bool Transformation::transform(const std::vector<Vertexes>* vec) {
  if (*curr_matrix == Matrix::get_identity()) {
    return false;
  }
  clear_vertexes();
  std::vector<Vertexes>::const_iterator iter = vec->cbegin();
  while (iter != vec->cend()) {
    transformed.push_back(transform(*iter, *curr_matrix));
    iter++;
  }
  return true;
}

void Transformation::clear_vertexes(void) {
  std::vector<Vertexes>::iterator iter = transformed.begin();
  while (iter != transformed.end()) {
    if (iter->vx) {
      free(iter->vx);
      iter->vx = nullptr;
    }
    if (iter->vy) {
      free(iter->vy);
      iter->vy = nullptr;
    }
    iter++;
  }
  transformed.clear();
}
