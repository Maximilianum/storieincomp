/*
 * motion.cc
 * Copyright (C) 2020 Massimiliano Maniscalco
 * 
 * This file is part of DoppiAvventura
 *
 * This is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "motion.hh"

bool Motion::move(SDL_Rect* pos) {
  if (!timing.check_time()) {
    return false;
  }
  std::pair<double,double> step = get_step(pos);
  pos->x += step.first;
  pos->y += step.second;
  return step.first != 0.0 && step.second != 0.0;
}

std::pair<double,double> Motion::get_step(const SDL_Rect* pos) {
  if (destinations.size() > 0) {
    int dx = destinations.front().x - pos->x;
    int dy = destinations.front().y - pos->y;
    int pwd = std::pow(dx, 2.0) + std::pow(dy, 2.0);
    if (pwd > 0) {
      double spc = std::sqrt(pwd);
      double spd = std::sqrt(std::pow(speed.x,2.0) + std::pow(speed.y,2.0));
      double time = spc / spd;
      std::pair<double, double> step { dx / time, dy / time };
      if (std::fabs(step.first) > std::abs(dx)) {
	step.first = dx;
      }
      if (std::fabs(step.second) > std::abs(dy)) {
	step.second = dy;
      }
      return step;
    } else {
      destinations.erase(destinations.begin());
    }
  }
  return std::pair<double,double>{0.0,0.0};
}
