/*
 * fading.cc
 * Copyright (C) 2020 Massimiliano Maniscalco
 * 
 * This file is part of DoppiAvventura
 *
 * This is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "fading.hh"

void Fading::set_mode(Mode md) {
  mode = md;
  switch (mode) {
  case Mode::in:
  case Mode::in_loop:
    pace = std::abs(pace);
    break;
  case Mode::out:
  case Mode::out_loop:
    pace = std::abs(pace) * -1.0;
    break;
  }
}

void Fading::set_pace(double val) {
  if (std::abs(val) > 64.0) {
    pace = 64.0;
  } else {
    pace = val;
  }
  set_mode(mode); // this will adjust pace sign
}

bool Fading::fade(void) {
  if (mode == Mode::none || !timing.check_time()) {
    return false;
  }
  alpha += pace;
  if (pace > 0.0) {
    pace += acceleration;
  } else {
    pace -= acceleration;
  }
  switch (mode) {
  case Mode::in:
    if (alpha > 255.0) {
      alpha = 255.0;
      mode = Mode::none;
    }
    break;
  case Mode::out:
    if (alpha < 0.0) {
      alpha = 0.0;
      mode = Mode::none;
    }
    break;
  case Mode::in_out:
    if (alpha > 255.0) {
      alpha = 254.0;
      pace = std::abs(pace) * -1.0;
    } else if (alpha < 0.0) {
      alpha = 0.0;
      mode = Mode::none;
    }
    break;
  case Mode::in_loop:
    if (alpha > 255) {
      alpha = 0;
    }
    break;
  case Mode::out_loop:
    if (alpha < 0) {
      alpha = 255;
    }
    break;
  case Mode::in_out_loop:
    if (alpha > 255) {
      alpha = 254;
      pace = std::abs(pace) * -1.0;
    } else if (alpha < 0) {
      alpha = 1;
      pace = std::abs(pace);
    }
    break;
  }
  return true;
}
