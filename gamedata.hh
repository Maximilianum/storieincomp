/*
 * gamedata.hh
 * Copyright (C) 2020 Massimiliano Maniscalco
 * 
 * This is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _GAMEDATA_H_
#define _GAMEDATA_H_

#include <iostream>
#include <vector>
#include "story.hh"

class GameData {
  friend std::istream& operator>>(std::istream& is, std::vector<std::string> str_vec);
 public:
  static GameData* load_stories(const std::string& path);
  GameData();
  void set_stories(const std::vector<std::string>& vec);
  const std::vector<std::string>* get_stories(void) const { return &stories; }
  inline void set_path(const std::string& str) { path = str; }
  void load_titles(void);
  const std::vector<std::string>* get_titles(void) const { return &titles; }
  inline void set_current_story_index(int val) { curr_story = val; }
  inline int get_current_story_index(void) const { return curr_story; }
  void load_story(void);
  inline std::string get_current_title(void) { return story.get_title(); }
  inline std::string get_current_sentence(void) { return story.get_current_sentence(); }
  inline std::string get_first_half_sentence(void) { return story.get_first_half_sentence(); }
  inline std::string get_current_spell_path(void) { return story.get_current_spell_path(); }
  inline int get_current_sentence_index(void) const { return story.get_current_index(); }
  inline bool set_current_sentence_index(int index) { return story.set_current_index(index); }
  inline bool next_sentence(void) { return story.next_sentence(); }
  inline std::string get_current_word(void) const { return story.get_current_word(); }
  inline int get_story_size(void) const { return story.size(); }
  inline bool check_current_word(const std::string& str) { return story.check_current_word(str); }
  void clear(void);
  inline void log(void) { story.log(); }
 private:
  std::string path;
  std::vector<std::string> stories;
  std::vector<std::string> titles;
  int curr_story;
  Story story;
};

#endif // _GAMEDATA_H_

