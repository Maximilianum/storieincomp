/*
 * storyrow.h
 * Copyright (C) 2020 Massimiliano Maniscalco
 * 
 * This is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _STORYROW_H_
#define _STORYROW_H_

#include <iostream>
#include <string>
#include <SDL2/SDL.h>
#include <SDL2/SDL_mixer.h>

class StoryRow {
  friend std::istream& operator>>(std::istream& is, StoryRow& sr);
 public:
  StoryRow();
  ~StoryRow();
  inline std::string get_sentence(void) const { return sentence; }
  inline std::string get_word(void) const { return word; }
  inline bool check(const std::string& str) const { return word.compare(str) == 0; }
  std::string get_spaced_sentence(void) const;
  std::string get_first_half(void) const;
  
 private:
  std::string sentence;
  std::string word;
  Mix_Chunk* sound;
};

#endif // _STORYROW_H_
