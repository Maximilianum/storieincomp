/*
 * matrix.cc
 * Copyright (C) 2020 Massimiliano Maniscalco
 * 
 * This is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "matrix.hh"
#include <iostream>

Matrix::Matrix() {
  matrix[0][0] = 1.0;
  matrix[0][1] = 0.0;
  matrix[0][2] = 0.0;
  matrix[1][0] = 0.0;
  matrix[1][1] = 1.0;
  matrix[1][2] = 0.0;
  matrix[2][0] = 0.0;
  matrix[2][1] = 0.0;
  matrix[2][2] = 1.0;
}

Matrix::Matrix(std::initializer_list<double> values) {
  if (values.size() > 9) {
    throw MatrixError { std::string { "MatrixError exception: too many values in initializer list" } };
  }
  int i = 0;
  for (int r = 0; r < 3; r++) {            // iter this matrix rows
    for (int c = 0; c < 3; c++) {          // iter this matrix columns
      if (i < values.size()) {
	matrix[r][c] = values.begin()[i];
      } else {
	matrix[r][c] = 0.0;
      }
      i++;
    }
  }
}

Matrix& Matrix::operator=(const Matrix& mtx) {
  for (int r = 0; r < 3; r++) {            // iter this matrix rows
    for (int c = 0; c < 3; c++) {          // iter this matrix columns
      matrix[r][c] = mtx.matrix[r][c];
    }
  }
  return *this;
}

Matrix& Matrix::operator*(const Matrix& mtx) {
  for (int r1 = 0; r1 < 3; r1++) {            // iter this matrix rows
    double rcopy[] { matrix[r1][0], matrix[r1][1], matrix[r1][2] };
    for (int c2 = 0; c2 < 3; c2++) {          // iter other matrix columns
      double s = 0.0;
      for (int r2 = 0; r2 < 3; r2++) {        // iter other matrix rows
	s += rcopy[r2] * mtx.matrix[r2][c2];
      }
      matrix[r1][c2] = s;
    }
  }
  return *this;
}

std::vector<double> Matrix::operator*(const std::vector<double>& values) {
  std::vector<double> vec;
  if (values.size() == 3) {
    for (int r = 0; r < 3; r++) {               // iter this matrix rows
      double s = 0.0;
      for (int c = 0; c < 3; c++) {             // iter matrix columns and vector
	s += matrix[r][c] * values[c];
      }
      vec.push_back(s);
    }
  } else {
    throw MatrixError { std::string { "MatrixError exception: vector must have 3 elements" } };
  }
  return vec;
}

bool Matrix::operator==(const Matrix& mtx) {
  for (int r = 0; r < 3; r++) {            // iter matrix's rows
    for (int c = 0; c < 3; c++) {          // iter matrix's columns
      if (matrix[r][c] != mtx.matrix[r][c]) {
	return false;
      }
    }
  }
  return true;
}

void Matrix::log(void) const {
  for (int r = 0; r < 3; r++) {            // iter this matrix rows
    for (int c = 0; c < 3; c++) {          // iter this matrix columns
      std::cout << matrix[r][c] << " ";
    }
    std::cout << std::endl;
  }
}
