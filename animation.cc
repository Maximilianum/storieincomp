/*
 * animation.cc
 * Copyright (C) 2020 Massimiliano Maniscalco
 *
 * This file is part of DoppiAvventura
 * 
 * TinyTextGame is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * TinyTextGame is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "animation.hh"

bool Animation::animate(void) {
  if (mode == Mode::none || !timing.check_time()) {
    return false;
  }
  current_frame += way;
  switch (mode) {
  case Mode::oneway:
    if (current_frame >= total_frames) {
      current_frame = 0;
      if (repetition > 0) {
	repetition--;
      } else {
	mode = Mode::none;
      }
    }
    return true;
  case Mode::twoway:
    if (current_frame >= total_frames) {
      current_frame = total_frames - 1;
      way = -1;
    } else if (current_frame < 0) {
      current_frame = 0;
      way = 1;
      if (repetition > 0) {
	repetition--;
      } else {
	mode = Mode::none;
      }
    }
    return true;
  case Mode::loop1way:
    if (current_frame >= total_frames) {
      current_frame = 0;
    }
    return true;
  case Mode::loop2way:
    if (current_frame >= total_frames) {
      current_frame = total_frames - 1;
      way = -1;
    } else if (current_frame < 0) {
      current_frame = 0;
      way = 1;
    }
    return true;
  }
  return false;
}
