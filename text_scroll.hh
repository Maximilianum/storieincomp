/*
 * text_scroll.h
 * Copyright (C) 2020 Massimiliano Maniscalco
 * 
 * This file is part of DoppiAvventura
 *
 * This is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _TEXT_SCROLL_H_
#define _TEXT_SCROLL_H_

#include <vector>
#include "text.hh"

class TextScroll {
 public:
  enum class Alignment { left, centre };
  TextScroll();
  ~TextScroll();
  inline void set_rect(const SDL_Rect& r) { rect = r; }
  inline SDL_Rect get_rect(void) const { return rect; }
  inline void set_max_rows(int val) { max_rows = val; }
  inline int get_max_rows(void) const { return max_rows; }
  void add_row(Text* txt, Alignment al = Alignment::left);
  void set_text(const std::vector<Text*>& vec, Alignment al = Alignment::left);
  inline int get_number_of_rows(void) const { return rows.size(); }
  const Text* get_last_row(void) const;
  inline bool is_over_max_rows(void) const { return rows.size() >= max_rows; }
  static void fade_out(std::vector<Text*>* vec, int n);
  inline void fade_out(int n) { fade_out(&rows, n); }
  static void scroll(std::vector<Text*>* vec);
  inline void scroll(void) { scroll(&rows); }
  bool is_ready(void);
  void update(void);
  void update_autoscroll(void);
  void draw(void);
  void clear_rows(void);
 private:
  SDL_Rect rect;
  int max_rows;
  std::vector<Text*> rows;
};

#endif // _TEXT_SCROLL_H_
