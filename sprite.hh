/*
 * sprite.h
 * Copyright (C) 2020 Massimiliano Maniscalco
 * 
 * This file is part of DoppiAvventura
 *
 * This is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SPRITE_H_
#define _SPRITE_H_

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <string>
#include "animation.hh"
#include "motion.hh"
#include "fading.hh"

class Sprite {
  
public:
  
  class SpriteError {  
  public:
    SpriteError(const std::string& str) : message (str) {}
    inline std::string get_message(void) const { return message; }
  private:
    const std::string message;
  };
  
  Sprite(SDL_Renderer* rndr, SDL_Texture* txtr);
  ~Sprite() {}
  inline void set_source_rect(const SDL_Rect& rect) { source = rect; }
  inline SDL_Rect get_source_rect(void) const { return source; }
  inline void set_current_frame(int val) { anime.set_current_frame(val); }
  inline int get_current_frame(void) const { return anime.get_current_frame(); }
  inline void set_total_frames(int val) { anime.set_total_frames(val); }
  inline int get_total_frames(void) const { return anime.get_total_frames(); }
  inline void set_position(const SDL_Rect& rect) { position = rect; }
  inline SDL_Rect get_position(void) const { return position; }
  inline void add_destination(const SDL_Point& pnt) { motion.add_destination(pnt); }
  inline void clear_destinations(void) { motion.clear_destinations(); }
  inline bool is_moving(void) const { return motion.is_moving(); }
  inline void set_speed(const Motion::Speed& spd) { motion.set_speed(spd); }
  inline Motion::Speed get_speed(void) const { return motion.get_speed(); }
  inline void set_animation_mode(Animation::Mode md) { anime.set_mode(md); }
  inline Animation::Mode get_animation_mode(void) const { return anime.get_mode(); }
  inline void set_animation_reps(unsigned short val) { anime.set_repetition(val); }
  inline void set_visible(bool flag) { visible = flag; }
  inline bool is_visible(void) const { return visible; }
  inline void set_sensitive(bool flag) { sensitive = flag; }
  inline bool is_sensitive(void) const { return sensitive; }
  inline void set_alpha(unsigned char val) { fading.set_alpha(val); }
  inline char get_alpha(void) const { return fading.get_alpha(); }
  inline void set_fading_mode(Fading::Mode md) { fading.set_mode(md); }
  inline Fading::Mode get_fading_mode(void) const { return fading.get_mode(); }
  inline void set_fading_pace(double val) { fading.set_pace(val); }
  inline void set_fading_acceleration(double val) { fading.set_acceleration(val); }
  bool check_position(const SDL_Point* sp);
  void draw(void);
  inline bool animate(void) { return anime.animate(); }
  inline bool move(void) { return motion.move(&position); }
  inline bool fade(void) { return fading.fade(); }
  
private:
  SDL_Renderer* renderer;
  SDL_Texture* texture;
  Animation anime;
  SDL_Rect source;
  SDL_Rect position;
  Motion motion;
  bool visible;
  bool sensitive;
  Fading fading;
};

#endif // _SPRITE_H_
