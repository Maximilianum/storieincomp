/*
 * drawing.h
 * Copyright (C) 2020 Massimiliano Maniscalco
 * 
 * This is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _DRAWING_H_
#define _DRAWING_H_

#include <SDL2/SDL.h>
#include <vector>
#include "timing.hh"

class Drawing {
 public:
  enum class Mode { up, down };
  Drawing(void);
  inline void set_renderer(SDL_Renderer* rndr) { renderer = rndr; }
  inline void set_width(int val) { width = val; }
  inline void set_height(int val) { height = val; }
  inline void set_number_patterns(int val) { nrpt = val; }
  void set_pattern(const std::vector<std::pair<int, SDL_Colour>>& vec);
  inline void set_background_colour(const SDL_Colour& col) { backcol = col; }
  void set_lines(const std::vector<std::pair<SDL_Rect, SDL_Colour>>& vec);
  void create_pattern(void);
  void animate_pattern(Mode mode = Mode::up);
  void draw(void);
 private:
  SDL_Renderer* renderer;
  int width;
  int height;
  int nrpt; // number of patterns
  std::vector<std::pair<int, SDL_Colour>> pattern;
  SDL_Colour backcol;
  std::vector<std::pair<SDL_Rect, SDL_Colour>> lines;
  Timing timing;
};

#endif // _DRAWING_H_
