/*
 * sdlengine.hh
 * Copyright (C) 2020 Massimiliano Maniscalco
 * 
 * This file is part of DoppiAvventura
 *
 * This is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SDLENGINE_H_
#define _SDLENGINE_H_

#include <iostream>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_mixer.h>
#include <vector>
#include <map>
#include <chrono>
#include <random>
#include <fontconfig/fontconfig.h>
#include "gamedata.hh"
#include "text.hh"
#include "text_scroll.hh"
#include "drawing.hh"
#include "sketch.hh"
#include "sprite.hh"
#include "player.hh"

class SDLEngine {

  enum class SketchesSchema { none, waterfall, fireworks };
  enum class GameStatus { quit, title, loading, starting, game, endstory, breakstory, saving };
  
public:
  // paths
#define SOUNDS_PATH   std::string("sounds/")
#define SPELLS_PATH   std::string("sounds/spells/")
#define GRAPHICS_PATH std::string("graphics/")
  // fonts
#define FONT_SIZE_TITLE 20
#define FONT_SIZE_STORY 30
#define FONT_SIZE_BIG   60
  // textures
#define TXTR_BOOKS      0
#define TXTR_BIG_COIN   1
#define TXTR_SPEAKER    2
#define TXTR_SMALL_COIN 3
  // sprites
#define SPRITE_BOOKS    0
#define SPRITE_COINS    1
#define SPRITE_SPEAKER  2
  // colours
#define COL_TXT_CREDITS  {255,255,255,255}
#define COL_TXT_COINS    {255,240,0,255}
#define COL_TXT_GAME     {0,0,0,255}
#define COL_TXT_RIGHT    {0,0,255,255}
#define COL_TXT_WRONG    {255,0,0,255}
#define COL_TXT_DISABLED {0,0,0,50}
  // labels
#define LABEL_TITLE    0
#define LABEL_COINS    1
#define LABEL_CURRENT  2
#define LABEL_MESSAGE  3
  // sounds
#define SOUND_SPELL       0
#define SOUND_COIN_EARNED 1
#define SOUND_KIDS_YEAH   2
#define SOUND_KID_NO      3
  // sound channels
#define CHANNEL_SPELL  0
#define CHANNEL_COINS  1
#define CHANNEL_KIDS   2
  // musics
#define TITLE_MUSIC  0
#define GAME_MUSIC   1
#define TITLE_MUSIC_VOLUME 64
#define GAME_MUSIC_VOLUME 24
#define GAME_MUSIC_LOW_VOLUME 12

  SDLEngine(const std::string& p, bool dm = false) : main_path (p), debugmode (dm), window (nullptr), win_width (1280), win_height (1024), renderer (nullptr), story_font (nullptr), title_font (nullptr), big_font (nullptr), gamestatus (GameStatus::title), gdata (nullptr), sketches_delay (2000.0) {}
  ~SDLEngine();
  int start(void);

protected:
  void create_dirs(void);
  static inline void on_SDL_error(std::ostream &os, const std::string &msg) { os << msg << " error: " << SDL_GetError() << std::endl; }
  std::string find_font_path(const std::string& name, const FcChar8* type);
  bool init(void);
  void init_title(void);
  void init_save_slots_menu(void);
  void init_starting(void);
  void init_game(void);
  void init_report(void);
  void on_title_event(SDL_Event* event);
  void on_loading_event(SDL_Event* event);
  void on_starting_event(SDL_Event* event);
  void on_game_event(SDL_Event* event);
  void on_report_event(SDL_Event* event);
  void on_saving_event(SDL_Event* event);
  void title_render(void);
  void loading_render(void);
  void starting_render(void);
  void game_render(void);
  void report_render(void);
  void saving_render(void);
  void check_word(const std::string& str);
  void check_sprites_click(SDL_Event* event);
  static void channel_spell_done(int chn);
  static void music_done(void);
  bool load_game(short slot);
  bool save_game(short slot);
  bool check_slot(short slot);
  bool add_sentence(void);
  void set_current_label_position(void);
  void update_current_label(void);
  void clone_current_label(bool correct);
  void update_speaker_position(void);
  void add_small_coin(void);
  void update_fading_label(int index, Fading::Mode mode);
  void add_random_sketches(SketchesSchema mode);
  void set_waterfall_route(Sketch* sktc);
  void set_fireworks_route(Sketch* sktc);
  void draw_sketches(void);
  void update_sketches(SketchesSchema mode = SketchesSchema::none, bool destroy = false);
  void draw_labels(void);
  void update_labels(bool destroy = false);
  void draw_sprites(void);
  void update_sprites(void);
  void update_coins(void);
  void cleanup_data(void);
  void free_sound_spell(void);
  void free_memory(void);
  void free_sketches(void);
  void free_labels(void);
  void free_sprites(void);
  void free_textures(void);
  void free_sounds(void);
  void free_musics(void);

private:
  SDL_Window* window;
  int win_width;
  int win_height;
  SDL_Renderer* renderer;

  // paths
  std::string main_path;
  std::string save_path;
  
  // fonts
  TTF_Font* story_font;
  TTF_Font* title_font;
  TTF_Font* big_font;
  
  // sounds & music
  std::map<char,Mix_Chunk*> sounds;
  static std::map<char,Mix_Music*> musics;
  static char current_music;
  static int music_volume;
  
  // game Text
  std::map<char,Text*> map_labels;
  std::vector<Text*> labels;
  TextScroll titlescroll;
  TextScroll storyscroll;

  // drawings
  Drawing* gamedraw;
  
  // textures
  std::map<int,SDL_Texture*> textures;

  // sketches
  std::vector<Sketch*> sketches;
  std::chrono::high_resolution_clock::time_point sketches_last;
  double sketches_delay;
  
  // game sprites
  std::map<char,Sprite*> sprites;
  std::vector<Sprite*> temp_sprites;
  
  // game data
  GameStatus gamestatus;
  bool debugmode;
  GameData* gdata;
  int label_index; // index of selected label
  std::string keybuffer;
  Player player;
  int combo; // combo counter
  
};

#endif // _SDLENGINE_H_

