/*
 * sketch.h
 * Copyright (C) 2020 Massimiliano Maniscalco
 * 
 * This is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SKETCH_H_
#define _SKETCH_H_

#include <SDL2/SDL.h>
#include <SDL2/SDL2_gfxPrimitives.h>
#include <string>
#include <vector>
#include "transformation.hh"
#include "animation.hh"
#include "motion.hh"
#include "fading.hh"

#define PI 3.14159265

class Sketch {
 public:
  static Transformation::Vertexes create_star(int x, int y, int size, const SDL_Colour& col);
  static std::vector<Transformation::Vertexes> create_flower(int x, int y, int size, const SDL_Colour& col1, const SDL_Colour& col2);
  
  class SketchError {  
  public:
    SketchError(const std::string& str) : message (str) {}
    inline std::string get_message(void) const { return message; }
  private:
    const std::string message;
  };
  
  Sketch(SDL_Renderer* rndr, const SDL_Rect& src);
  ~Sketch();
  inline void set_renderer(SDL_Renderer* rndr) { renderer = rndr; }
  inline void set_source_rect(const SDL_Rect& rect) { source = rect; }
  inline SDL_Rect get_source_rect(void) const { return source; }
  inline void set_position(const SDL_Rect& rect) { position = rect; }
  inline SDL_Rect get_position(void) const { return position; }
  void set_drawings(const std::vector<Transformation::Vertexes>& vec);
  inline void add_drawing(const Transformation::Vertexes& sd) { drawings.push_back(sd); }
  inline void add_drawings(const std::vector<Transformation::Vertexes>& vec) { drawings.insert(drawings.end(), vec.begin(), vec.end()); }
  inline void set_current_transformation(int index) { transformation.set_current_matrix(index); }
  void add_transformation(void);
  inline int count_transformations(void) const { return transformation.count(); }
  inline void apply_translation(double cx, double cy, int index = 0) { transformation.mult_by_translation(cx, cy, index); }
  inline void apply_rotate(double a, int index = 0) { transformation.mult_by_rotate(a, index); }
  inline void apply_scale(double cx, double cy, int index = 0) { transformation.mult_by_scale(cx, cy, index); }
  inline void apply_shear(double cx, double cy, int index = 0) { transformation.mult_by_shear(cx, cy, index); }
  void clear_transformations(void);
  bool transform(void);
  inline void set_current_frame(int val) { anime.set_current_frame(val); }
  inline int get_current_frame(void) const { return anime.get_current_frame(); }
  inline void set_animation_mode(Animation::Mode md) { anime.set_mode(md); }
  inline Animation::Mode get_animation_mode(void) const { return anime.get_mode(); }
  inline void set_animation_reps(unsigned short val) { anime.set_repetition(val); }
  inline void add_destination(const SDL_Point& pnt) { motion.add_destination(pnt); }
  inline void clear_destinations(void) { motion.clear_destinations(); }
  inline bool is_moving(void) const { return motion.is_moving(); }
  inline void set_speed(const Motion::Speed& spd) { motion.set_speed(spd); }
  inline Motion::Speed get_speed(void) const { return motion.get_speed(); }
  inline void set_alpha(unsigned char val) { fading.set_alpha(val); }
  inline char get_alpha(void) const { return fading.get_alpha(); }
  inline void set_fading_mode(Fading::Mode md) { fading.set_mode(md); }
  inline Fading::Mode get_fading_mode(void) const { return fading.get_mode(); }
  inline void set_fading_pace(double val) { fading.set_pace(val); }
  inline void set_fading_acceleration(double val) { fading.set_acceleration(val); }
  void update_texture(void);
  void draw(void);
  bool animate(void);
  inline bool move(void) { return motion.move(&position); }
  inline bool fade(void) { return fading.fade(); }
protected:
  void clear_drawings(void);
  static SDL_Texture* create_texture(const Sketch& sktc);
  static Transformation::Vertexes create_circle(int x, int y, int r, int np, const SDL_Colour& col);
  static Transformation::Vertexes create_ellipse(int x, int y, double a, double b, int np, const SDL_Colour& col);
 private:
  Sketch() : renderer (nullptr), texture (nullptr) {}
  SDL_Renderer* renderer;
  SDL_Texture* texture;
  SDL_Rect source;
  SDL_Rect position;
  std::vector<Transformation::Vertexes> drawings;
  Animation anime;
  Transformation transformation;
  Motion motion;
  Fading fading;
};

#endif // _SKETCH_H_
