
/*
 * player.cc
 * Copyright (C) 2020 Massimiliano Maniscalco
 * 
 * This is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "player.hh"
#include <sstream>
#include <fstream>
#include <iostream>

std::ostream& operator<<(std::ostream& os, const std::vector<std::vector<bool>>& vec) {
  std::vector<std::vector<bool>>::const_iterator it_stories = vec.begin(); // iter stories
  while (it_stories != vec.end()) {
    os << it_stories->size() << ' '; // save number of sentence at the start of the story
    if (!os.good()) {
      return os;
    }
    std::vector<bool>::const_iterator it_sentences = it_stories->begin(); // iter sentences
    while (it_sentences != it_stories->end()) {
      os << *it_sentences;
      if (!os.good()) {
	return os;
      }
      it_sentences++;
      if (it_sentences != it_stories->end()) {
	os << ' ';
	if (!os.good()) {
	  return os;
	}
      }
    }
    os << '\n';
    if (!os.good()) {
      return os;
    }
    it_stories++;
  }   
  return os;
}

std::ostream& operator<<(std::ostream& os, const std::vector<bool>& vec) {
  os << vec.size() << '\n';
  if (!os.good()) {
    return os;
  }
  std::vector<bool>::const_iterator iter = vec.begin();
  while (iter != vec.end()) {
    os << *iter;
    iter++;
    if (iter != vec.end()) {
      os << ' ';
      if (!os.good()) {
	return os;
      }
    }
  }
  os << '\n';
  return os;
}

std::istream& operator>>(std::istream& is, std::vector<std::vector<bool>>& vec) {
  while (is.good()) {
    int size = 0;
    is >> size; // read story size (number of sentence)
    if (!is.good()) {
      return is;
    }
    if (size > 0) {
      vec.push_back(std::vector<bool>());
      for (int n = 0; n < size; n++) {
	bool r;
	is >> r;
	if (!is.good()) {
	  return is;
	}
	vec.back().push_back(r);
      }
    }
  }   
  return is;
}

std::istream& operator>>(std::istream& is, std::vector<bool>& vec) {
  int size = 0;
  is >> size; // read unlocked stories size
  if (!is.good()) {
    return is;
  }
  for (int i = 0; i < size; i++) {
    bool val;
    is >> val;
    if (is.good()) {
      vec.push_back(val);
    } else {
      return is;
    }
  }
  return is;
}

Player::Player() : coins (0), curr_story (0), curr_correct (0) {
  //add_story();
}

void Player::save_result(int index, bool flag) {
  try {
    if (flag) {
      curr_correct++;
    }
    if (index < results.at(curr_story).size()) {
      if (flag) {
	results.at(curr_story).at(index) = flag;
      }
    } else {
      results.at(curr_story).push_back(flag);
    }
  } catch (const std::out_of_range& oor) {
    throw PlayerError { std::string { "PlayerError::save_result() out of range" } };
  }
}

bool Player::check_result(int index) {
  try {
    if (index < results.at(curr_story).size()) {
      return results.at(curr_story).at(index);
    }
  } catch (const std::out_of_range& oor) {
    throw PlayerError { std::string { "PlayerError::check_result() out of range" } };
  }
  return false;
}

int Player::unlock_story(void) {
  stories.push_back(true);
  return stories.size() - 1;
}

bool Player::save(const std::string& path, int curr_sentence_index) {
  bool result = false;
  std::ofstream outfile (path, std::ofstream::out);
  result = outfile.good();
  if (result) {
    outfile << curr_story << ' '; // save current story
    result = outfile.good();
    if (result) {
      outfile << curr_sentence_index << ' '; // save current sentence index
      result = outfile.good();
      if (result) {
	outfile << curr_correct << std::endl; // save current correct
	result = outfile.good();
	if (result) {
	  outfile << coins << std::endl; // save coins
	  result = outfile.good();
	  if (result) {
	    outfile << stories; // save vector of unlocked stories
	    result = outfile.good();
	    if (result) {
	      outfile << results; // save vectors of results
	      result = outfile.good();
	    }
	  }
	}
      }
    }
  }
  outfile.close();
  return result;
}

bool Player::load(const std::string& path, int* curr_sentence_index) {
  bool result = false;
  stories.clear();
  results.clear();
  std::ifstream infile (path, std::ifstream::in);
  result = infile.good();
  if (result) {
    infile >> curr_story; // load current story
    result = infile.good();
    if (result) {
      infile >> *curr_sentence_index; // load current sentence index
      result = infile.good();
      if (result) {
	infile >> curr_correct; // load current correct
	result = infile.good();
	if (result) {
	  infile >> coins; // load coins
	  result = infile.good();
	  if (result) {
	    infile >> stories; // load vector of unlocked stories
	    result = infile.good();
	    if (result) {
	      infile >> results; // load vector of results
	      result = infile.good();
	    }
	  }
	}
      }
    }
  }
  if (infile.fail() && !infile.eof()) {
    std::cerr << "load error file " << path << std::endl;
  } else {
    result = true;
  }
  infile.close();
  return result;
}

void Player::log(void) const {
  std::cout << "current story: " << curr_story << std::endl;
  std::cout << "current corrected: " << curr_correct << std::endl;
  int stn = 0;
  std::vector<bool>::const_iterator itsl = stories.cbegin();
  while (itsl != stories.cend()) {
    std::cout << "story number " << stn << " is unlocked." << std::endl;
    stn++;
    itsl++;
  }
  stn = 0;
  int sen = 0;
  std::vector<std::vector<bool>>::const_iterator iter = results.begin();
  while (iter != results.end()) {
    std::cout << "story number " << stn << std::endl;
    std::vector<bool>::const_iterator it = iter->begin();
    while (it != iter->end()) {
      std::cout << "\tanswer number\t" << sen << "\t";
      if (*it) {
	std::cout << "true" << std::endl;
      } else {
	std::cout << "false" << std::endl;
      }
      sen++;
      it++;
    }
    stn++;
    iter++;
  }
}
