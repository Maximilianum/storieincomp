/*
 * motion.h
 * Copyright (C) 2020 Massimiliano Maniscalco
 * 
 * This file is part of DoppiAvventura
 *
 * This is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _MOTION_H_
#define _MOTION_H_

#include "timing.hh"
#include <vector>

class Motion {
 public:
  typedef struct {
    double x;
    double y;
  } Speed;
 Motion() : speed ({0.0,0.0}) {}
  inline void add_destination(const SDL_Point& pnt) { destinations.push_back(pnt); }
  inline void clear_destinations(void) { destinations.clear(); }
  inline bool is_moving(void) const { return destinations.size() > 0; }
  inline void set_speed(const Speed& spd) { speed = spd; }
  inline Speed get_speed(void) const { return speed; }
  bool move(SDL_Rect* pos);
 protected:
  std::pair<double,double> get_step(const SDL_Rect* pos);
 private:
  Timing timing;
  std::vector<SDL_Point> destinations;
  Speed speed;
  
};

#endif // _MOTION_H_
