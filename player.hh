/*
 * player.hh
 * Copyright (C) 2020 Massimiliano Maniscalco
 * 
 * This is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _PLAYER_H_
#define _PLAYER_H_

#include <string>
#include <vector>

class Player {
  friend std::ostream& operator<<(std::ostream& os, const std::vector<std::vector<bool>>& vec);
  friend std::ostream& operator<<(std::ostream& os, const std::vector<bool>& vec);
  friend std::istream& operator>>(std::istream& is, std::vector<std::vector<bool>>& vec);
  friend std::istream& operator>>(std::istream& is, std::vector<bool>& vec);
 public:
  
  class PlayerError {  
  public:
    PlayerError(const std::string& str) : message (str) {}
    inline std::string get_message(void) const { return message; }
  private:
    const std::string message;
  };
  
  Player();
  inline void set_coins(unsigned int val) { coins = val; }
  inline unsigned int get_coins(void) const { return coins; }
  inline void add_coin(void) { coins++; }
  inline void add_story(void) { results.push_back(std::vector<bool>()); }
  inline bool check_story(int index) const { return index < results.size(); }
  inline void set_current_story_index(int val) { curr_story = val; curr_correct = 0; }
  inline int get_current_story_index(void) const { return curr_story; }
  void save_result(int index, bool flag);
  bool check_result(int index);
  inline int get_current_correct(void) const { return curr_correct; }
  int unlock_story(void);
  inline bool story_is_unlocked(int id) { return id < stories.size() && stories.at(id); }
  bool save(const std::string& path, int curr_sentence_index);
  bool load(const std::string& path, int* curr_sentence_index);
  void log(void) const;
 private:
  unsigned int coins;
  std::vector<std::vector<bool>> results; // saved results
  int curr_correct;
  int curr_story;
  std::vector<bool> stories; // unlocked stories
};

#endif // _PLAYER_H_
